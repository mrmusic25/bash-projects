#!/bin/bash
#
# internetCheck.sh - A script that monitors and reports local internet outages
#
# Usage: ./internetCheck.sh [--verbose]
#
# I recommend running this in a screen session with the verbose flag on
# 
# Changelog:
# v1.1.2
# - Updated debug() messages to use commonFunctions.sh v2.0.0
#
# v1.1.1
# - Fixed divide by zero error, script will no longer create black holes
# - Changed default reportMargin to 50% as 20% was triggering too often for my liking
# - Added a variable for the dateFormat when calling 'date', and changed format to be more readable
#
# TODO:
# - Make all stats an array in memory for speed
#   ~ Still output new data to log, just makes processing quicker
#   ~ Only time wasted is in beginning when importing old log
#
# v1.1.2, 21 July 2020 12:30 PDT

### Script setup

export shortName="iC"
export longName="internetCheck"

source /usr/share/commonFunctions.sh

### Vars

internetCheck=60 # Number of seconds to wait between successful internet checks
outageCheck=5 # Seconds between checks when internet outage detected
secondLog="$HOME/outage.txt" # Secondary log file for easy access
pingTimeout=2 # Seconds until (unsuccessful) ping times out
pingAddress="8.8.8.8" # Server to test ping. NOTE: domain names will work, but not recommended!
backupAddress="1.1.1.1" # Will try pinging this before declaring an outage, in case main is down or unresponsive
pingCount=5 # Number of pings to send before declaring failure (one successful ping will return as successful)
failMargin=2 # Margin of error, i.e how many failure must occur before reporting a failure
statPrefix="$HOME/.plog_" # Prefix to log output for statistics
reportMargin=50 # Percentage of margin to exceed before reporting ping (i.e. 20% will only report when ping >= 1.2 * average)
statMaxLines=100000 # Number of lines before ~/.plog_master is trimmed
outMaxLines=100000 # Same as above, but for outage.txt
dateFormat='+%d-%b-%Y_%H:%M:%S' # Format to use when calling date. Default is: "Jan-01-2020_18:57:23"

### Functions

# Returns 0 if internet is working, 1 if down (ping based, YMMV)
function internetUp() {
	# Prepare function variables
	local failCount=0
	local pcount=0
	local rval=0
	if [[ -z $1 ]]; then
		debug "l3" "No argument given to internetUp()! Please fix and re-run"
		return 1
	elif [[ -z $2 ]]; then
		debug "l2" "No stat file given to internetUp(), reported statistics may be inaccurate!"
	fi
	
	# Get stat output ready
	local outStat
	if [[ "$2" == *null ]]; then
		outStat="/dev/null"
	else
		outStat="$statPrefix""$2"
	fi

	while [[ $pcount < $pingCount ]]; do
		ping -c 1 -W "$pingTimeout" "$1" | tee -a "$outStat" 1>/dev/null
		rval=$?
		if [[ $rval -ne 0 ]]; then
			((failCount++))
		fi
		((pcount++))
	done
	return $failCount # No quotes so it registers as an int
}

# $1 - time in seconds internet was down
# Functions outputs to both logs, after converting time to minutes, and possibly hours
# Return values can be safely ignored
function logTime() {
	if [[ -z $1 ]]; then
		debug "l3" "No argument given to logTime()!"
		return 1
	elif [[ "$1" -eq 0 ]]; then
		debug "l5" "No outage, continuing!"
		return 0
	fi
	
	local message="On $(date --date "-$1 sec"), the internet was down for"

	local seconds="$1"
	local minutes=0
	local hours=0

	while [[ "$seconds" -gt 59 ]]; do
		((minutes++))
		((seconds-=60))
	done
	while [[ "$minutes" -gt 59 ]]; do
		((hours++))
		((minutes-=60))
	done

	if [[ "$hours" -gt 0 ]]; then
		message="$message $hours hours,"
	fi
	if [[ "$minutes" -gt 0 ]]; then
		message="$message $minutes minutes,"
	fi
	message="$message $seconds seconds."

	debug "l3" "$message"
 	echo "$message" | tee -a "$secondLog" 1>/dev/null
	return 0
}


function updateStats() {
	# Sanity check
	if [[ -z $1 ]]; then
		debug "l2" "No specific name given to updateStats(), output may be inaccurate!"
	elif [[ ! -f "$statPrefix""$1" ]]; then
		debug "l2" "Specific file $statPrefix$1 does not exist! Attempting to continue..."
	fi

	local sfile="$statPrefix""$1" # Ping data from the last test
	local mfile="$statPrefix""master" # Keeps all the ping data ever

	# Init mfile if it is empty
	touch "$mfile"
	
	# Grab all the timings from GNU/ping
	grep time= "$sfile" | rev | cut -d'=' -f 1 | rev | cut -d' ' -f 1 | cut -d'.' -f 1 | tee "$sfile" 1>/dev/null
	
	# Calculate the average time
	local total=0
	local sum=0
	local average=0
	while read -r line; do
		((total++))
		((sum+=line))
	done < "$mfile"
	((average=sum/total))
	
	# Now get average of last test
	local ctotal=0
	local csum=0
	local caverage=0
	while read -r line; do
		((ctotal++))
		((csum+=line))
	done < "$sfile"
	if [[ $ctotal -lt 1 ]]; then
		ctotal=1 # This fixes the divide by zero error that messes up statistics
	fi
	((caverage=csum/ctotal))
	
	# Get the average with a margin of error
	local rmargin=0
	((rmargin=((100+reportMargin)*average)/100))
	
	# Compare results and output accordingly
	# NOTE: This will trigger false-positive warnings until decent data has been collected
	if [[ $caverage -gt $rmargin ]]; then
		debug "l2" "Ping was higher than average at $1, $caverage ms vs $average ms"
		echo "WARN: Ping was higher than average at $1, $caverage ms vs $average ms" | tee -a "$secondLog" 1>/dev/null
	else
		debug "l1" "No outage detected, recent ping average is $caverage ms vs average $average ms"
	fi

	# To save disk space, only save the average of the last test to the master
	printf "%s\n" "$caverage" | tee -a "$mfile" 1>/dev/null
}

function trimFiles() {
	local mfile="$statPrefix""master" # Master file for ping averages
	
	# First, check master file since it is most likely to fill up
	if [[ $(wc -l "$mfile" | cut -d' ' -f1) -gt $statMaxLines ]]; then
		debug "l2" "$mfile has exceeded line limit at $(date), trimming!"
		# Get average of old file for first line of new one
		local mtotal=0
		local mcount=0
		while read -r line; do
			((mcount++))
			((mtotal+=line))
		done < "$mfile"
		local maverage=0
		((maverage=mtotal/mcount))

		# Remove previous backup, if it exists
		if [[ -e "$mfile".bak ]]; then
			rm "$mfile".bak
		fi
		mv "$mfile" "$mfile".bak

		# Finally, init new file
		printf "%s\n" $maverage | tee "$mfile" 1>/dev/null
	fi

	# Now do the same for outage, which should fill up much more slowly
	if [[ $(wc -l "$secondLog" | cut -d' ' -f1) -gt $outMaxLines ]]; then
		debug "l2" "$secondLog has exceeded line limit at $(date), trimming!"

		# Remove previous backup, if it exists
		if [[ -e "$secondLog".bak ]]; then
			rm "$secondLog".bak
		fi
		mv "$secondLog" "$secondLog".bak

		# No need to init the secondLog, done!
	fi
}

### Main

counter=0
while [[ 0 -eq 0 ]]; do
	cdate="$(date "$dateFormat")" # Set the current date for statistics
	while [[ $(internetUp "$pingAddress" "$cdate") -ge $failMargin ]]; do
		if [[ $(internetUp "$backupAddress" "/dev/null") -ge $failMargin ]]; then
			# Outage confirmed
			((counter+=outageCheck+pingCount*pingTimeout*2)) # Assumes all 3 pings fail, which they should have. More accurate time this way.
			sleep "$outageCheck"s
		fi
	done
	
	# Log the time if there was an error. Else, update latency stats
	if [[ "$counter" -eq 0 ]]; then
		updateStats "$cdate"
	else
		logTime "$counter"
	fi
	
	# Always reset your flags
	counter=0
	
	if [[ -f "$statPrefix$cdate" ]]; then
		rm "$statPrefix""$cdate"
	fi
	trimFiles

	sleep "$internetCheck"s
done

#EOF
