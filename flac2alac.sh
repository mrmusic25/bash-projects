#!/usr/bin/env bash

if [[ -z $1 ]]; then
    echo "Usage: flac2alac.sh <file>"
    echo "Use this script as the -exec argument for find!"
    exit 1
fi

file="$1"

if [[ ! -f "$file" ]]; then
    echo "Could not find given file $file!"
    exit 1
fi

out="$(echo "$file" | sed -e 's/.flac/.m4a/g')"
if ! ffmpeg -i "$file" -acodec alac -c:v copy "$out"; then
    echo "Could not convert $file to $out!"
    exit 1
else
    echo "Successfully converted $file to $out!"
    exit 0
fi

#EOF