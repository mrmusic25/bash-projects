#!/usr/bin/env bash
#
# combinSongs.sh - Script to combine two or more songs into one song of the same format using ffmpeg
#
# v0.0.4, 05 Aug. 2023, 22:17 PDT

### Prep

if ! source /usr/share/commonFunctions.sh; then
    if ! source commonFunctions.sh; then
        echo "Could not source commonFunctions.sh! Please install from https://gitlab.com/mrmusic25/linux-pref and try again!" 1>&2
        exit 1
    fi
fi

### Vars

extension=""             # File extension that each given file must match
tmpFile="$(pwd)/tmp.txt" # Text file where names of files to be passed to ffmpeg are kept
declare -a files         # Array of files to be combined

### Functions

function checkExt() {
    if [[ -z $1 ]]; then
        debug "l3" "No arguments given to checkExt()! Returning..."
        return 1
    fi

    local e="$(printf "$1" | rev | cut -d'.' -f1 | rev)"

    if [[ -z $extension ]]; then
        debug "l1" "Extension is not set! Using $e from file $1 as the extension for this run!"
        extension="$e"
        return 0
    elif [[ "$e" != "$extension" ]]; then
        debug "l3" "File $1 has a different extension than the one set: $extension! All files MUST have same extension to be combined! Please fix and re-run!"
        return 1
    else
        debug "l1" "File $1 has the correct extension $e! Moving on..."
        return 0
    fi

}
### Main

# Make sure args were given
if [[ -z $1 ]]; then
    debug "l4" "No arguments given to script! Please fix and re-run!"
    exit 1
fi

# Check if each argument exists, then add it to the array
while [[ -n "$1" ]]; do
    if [[ ! -f "$1" ]]; then
        debug "l4" "Given argument $1 is not a file! Please fix and re-run! Exiting..."
        exit 1
    elif ! checkExt "$1"; then
        debug "l4" "Extension for $1 is not valid! Please specify files of the same type to combine them! Exiting..."
        exit 1
    else
        debug "l1" "File $1 is valid, adding to the array!"
        files+=("$1")
    fi
    shift
done

# Delete old file
if [[ -f "$tmpFile" ]]; then
    debug "l1" "Deleting old temporary file $tmpFile..."
    rm -f "$tmpFile"
fi

# Now, add them each to the file
for file in "${files[@]}"; do
    printf "file \'%s\'\n" "$file" >> "$tmpFile"
done

newName="$(echo "${files[0]}" | rev | cut -d'/' -f 1 | cut -d '.' -f2 | rev)"
newName="$newName-combined.$extension"
debug "l1" "File name for the combined song is now: $newName"

# Now, run ffmpeg and combine
# Note: Convert to ALAC first, then combine! FLAC combination messes up timestamps
if ! ffmpeg -f concat -safe 0 -i "$tmpFile" -c:a copy "$newName"; then
    debug "l3" "Could not combine given files!"
else
    debug "l1" "Successfully combined files into $newName!"
    rm -f "$tmpFile" # Only delete upon successful conversion
fi

#EOF