#!/usr/bin/env bash
#
# updateMusic.sh - A script to update the music folder from a Samba server to local
#
# Usage: ./updateMusic.sh [-v] [-s]
#          -v turns on verbose mode
#          -s turns on setup mode
#
# Script is meant to be run in setup mode once, then run normally after
#
# v1.0.1, 20 Jan. 2021 15:20 PST

source /usr/share/commonFunctions.sh

### Functions

function setupScript() {
    # Clear the custom config first, assuming it exists
    myConfig "remove"

    # Now, set each option as it is entered
    local lib
    announce "Please enter the full path to your music library below." "Script will loop until valid folder is given" "Folder should be like '../iTunes/iTunes Media/Music'"
    until [[ -d "$lib" ]]; do
        read -p "Please enter the location of your music library: " lib
    done
    export localLibrary="$lib"
    myConfig add localLibrary

    local rlib
    announce "Please enter the server used for Samba" "e.g. If your server is \"10.100.100.75\" and the folder is \"Music\" type in \"//10.100.100.75/Music\"" "NOTE: This will not be error checked!"
    read -p "Please enter the server address and folder: " rlib
    export remoteLibrary="$rlib"
    myConfig add remoteLibrary

    local onamae
    read -p "Please enter the Samba username: " onamae
    export sambaUser="$onamae"
    myConfig add sambaUser

    local pass="1"
    local confirm="2"
    announce "Enter the password for the Samba user when prompted." "NOTE: Password will be stored in plaintext! (Limited to current user/group)" "If this concerns you, please make a read-only user for this script and re-setup!"
    until [[ "$pass" == "$confirm" ]]; do
        read -sp "Please enter the Samba user's password: " pass
        read -sp "Please confirm password: " confirm
    done
    export sambaPass="$pass"
    myConfig add sambaPass

    local ilib="null"
    announce "Please enter the full location to the 'Automatically Add to iTunes' folder." "In WSL, this is likely in '/mnt/c/User/<user>/Music/iTunes/iTunes Media' or similar"
    until [[ -d "$ilib" ]]; do
        read -p "Please enter the full path: " ilib
    done
    export itunesAddFolder="$ilib"
    myConfig add itunesAddFolder

    # Export config and continue
    myConfig "export"
}

### Main

if ! which smbinfo 1>/dev/null; then
    debug "l4" "cifs-utils not installed! Please fix and re-run!"
    exit 1
fi

# Args
while [[ -n $1 ]]; do
    if [[ "$1" == "-v" || "$1" == "--verbose" ]]; then
        export debugLevel=1
    elif [[ "$1" == "-s" || "$1" == "--setup" ]]; then
        setupScript
    else
        debug "l2" "Ignoring unknown option $1!"
    fi
    shift
done

# Import config
if ! myConfig import; then
    debug "l3" "No config file found! Please setup before continuing!"
    setupScript
fi

# Mount the samba folder, exit if unable
announce "Attempting to connect to server!" "Please provide sudo password if prompted for folder creation!"
if [[ ! -f /mnt/samba ]]; then
    debug "l3" "/mnt/samba does not exist, please provide sudo access to create folders!"
    sudo mkdir -pv /mnt/samba
    sudo chmod $USER:$USER /mnt/samba # So that current user can access it
    sudo chmod 0775 /mnt/samba
fi

announce "sudo privileges required to mount samba, please enter password at prompt!" "Attempting to auto-mount share!"
if ! sudo mount -t smbfs -o username="$sambaUser",password="$sambaPass" "$remoteLibrary" /mnt/samba; then
    debug "l4" "Samba server could not be connected! Please check settings and try again!"
    exit 1
fi

# Process folders
localIndex="$(pwd)/lindex.txt"
remoteIndex="$(pwd)/rindex.txt"
indexFolder "$localLibrary" "$localIndex"
indexFolder "$remoteLibrary" "$remoteIndex"
localDiff="$(pwd)/index.diff"

# Process the diff
diff "$localIndex" "$remoteIndex" | tee "$localDiff" 1>/dev/null
newSongs="$(pwd)/newSongs.txt"
deletedSongs="$(pwd)/deletedSongs.txt"
grep "<" "$localDiff" | cut -d'<' -f2 > "$newSongs"
grep ">" "$localDiff" | cut -d'>' -f2 > "$deletedSongs"

# Copy new files to the "Automatically Add to iTunes" folder
announce "Script will now copy new files to the given iTunes folder" "To confirm afterwards, the list is stored in $newSongs"
while read -r line; do
    debug "l1" "Copying $line to $itunesAddFolder"
    if ! cp "$line" "$itunesAddFolder"; then
        debug "l3" "Could not copy $line! Does $(whoami) have permission? Attempting to continue..."
    fi
done < "$newSongs"

# Notify user of deleted songs, if any
if [[ -n $(cat "$deletedSongs") ]]; then
    announce "Files were deleted from the server! Printng them now!" "User must manually delete these from iTunes." "For reference, these files are stored in $deletedSongs"
    while read -r line; do
        echo "$line"
    done < "$deletedSongs"
fi

# Cleanup
rm "$localIndex"
rm "$remoteIndex"
rm "$localDiff"
if ! sudo umount /mnt/samba; then
    debug "l3" "Could not unmount /dev/samba! Please do so manually!"
fi

debug "l1" "Done with script!"

#EOF