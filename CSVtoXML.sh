#!/usr/bin/env bash
#
# Imports a CSV file and converts it to XML
#
# NOTE: This assumes the first line of the file is the tag data!
#
# v1.0.4, 31 Jan. 2021 16:44 PST

if [[ -z $1 ]]; then
	echo "Please provide a file!"
	exit 1
elif [[ ! -f "$1" ]]; then
	echo "$1 is not a file!"
	exit 1
else
	inFile="$1"
fi

outFile="$(echo "$inFile" | rev | cut -d'.' -f1 --complement | rev).xml"

declare -a tags

# Get the number of tags and split them into tags[]
tagLine="$(cat "$inFile" | sed '1q;d')"
numTags="$(echo "$tagLine" | sed -e 's/[^,]//g' | wc -c)" # Gets the number of commas in the line
i=1
while [[ i -le $numTags ]]; do
	tags+=("$(echo "$tagLine" | cut -d',' -f $i)")
	((i++))
done

# Now, process each line
printf "<?xml version=\"1.0\"?>\n<data-set>\n" > "$outFile"
while read -r line; do
	if [[ -z $skipLine ]]; then # This allows us to skip the first line, where the tag data is kept (already imported)
		skipLine=0
	else
		printf "    <record>\n" | tee -a "$outFile" 1>/dev/null
		i=1
		for tag in "${tags[@]}"; do
			# Get whole tag
			key="$(echo "$line" | cut -d',' -f $i)"
			if [[ "$key" == \"* ]]; then # If the item has quotes, keep cutting until endquote is found
				until [[ "$key" == *\" ]]; do
					((i++))
					key="$key,$(echo "$line" | cut -d',' -f $i)"
				done
			fi
			
			# Remove newline/carriage return if detected
			if [[ "$tag" == *\n\r || "$tag" == *\r\n ]]; then 
				ltag="${tag::-2}" # Remove last two chars from string
			elif [[ "$tag" == *\n || "$tag" == *\r ]]; then
				ltag="${tag::-1}"
			else
				ltag="$tag"
			fi
			
			# Print full tag to the output file
			printf "        <%s>%s</%s>\n" "$ltag" "$key" "$ltag" | tee -a "$outFile" 1>/dev/null
			((i++))
		done
		printf "    </record>\n" | tee -a "$outFile" 1>/dev/null
	fi
done < "$inFile"
printf "</data-set>\n" | tee -a "$outFile" 1>/dev/null

#EOF
