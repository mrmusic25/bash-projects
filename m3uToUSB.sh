#!/bin/bash
#
# m3uToUSB - Script to convert all the songs in an m3u playlist to mp3 in an output directory
# A bash implementation of my Powershell script, for when bash is available on Windows
#
# Changes:
# v2.1.0
# - Deletion is tested and working!
# - Changed smoe small typos
# - Added placeholder for localization until I can actually work on it
#
# v2.0.2
# - Minor changes to make delete function work
# - Added unlisted option to ONLY delete songs (for testing, might implement later)
#
# v2.0.1
# - Added comm from coreutils as a requirement, removed parallel
# - Re-enabled copying of original m3u in importM3U()
# - Started work on localized playlist output
# - Added $origFile for use in comm, mainly for deleting files (later, may be implemented in quicker delta conversions)
# - Deleted old song deletion functions. Start from scratch!
# - Gave script its own $aChar for announce()
#
# v2.0.0
# - Started work on a faster, more efficient version of the script
# - Major change to importM3U(): it now appends the output file name to $newFile in real time
# - Not significant yet, but makes implementation of future plans easier
# - Changed converterLoop() so that duplicate names works now (untested)
#
# v1.2.2
# - Logic error with previous update, fixed it
#
# v1.2.1
# - Suprisingly easy implementation of duplicate song handling for outputFile()
# - Only took me two years to update this darn script lol. Gotta work on optimization and freezing issues on WSL soon...
#
# v1.2.0
# - Finished writing deleteOldSongs() and deleteFolderProcessor()
# - Added delete ability to script, gives a warnign beforehand
# - Like always, untested for now (gotta break this habit... one day...)
# - Minor text fixes
#
# TODO:
# - Great idea from this website to convert files in parallel, quick conversion! Requires ffmpeg, and moreutils
#   ~ https://wiki.archlinux.org/index.php/Convert_Flac_to_Mp3
# - Make it so CTRL+C adds current song to failedSongs[], then passes signal to ffmpeg (for broken conversions)
# - Create a file called ~/.m2uSettings.conf that gets preloaded.
#   ~ Keeps settings like prefix, output folder, defaults, etc.
# - For -d|--delete - Copy over current m3u, and compare m3u files to see what changed, and delete changes
# - Make 'secret' or 'unlisted' options only double options and list them in displayHelp()
#   ~ e.g: only --update instead of -u|--update
#   ~ Change timeout value of ffmpeg with --timeout <seconds>
#   ~ Useful for things like minor script options. Also useful in other scripts
#   ~ This almost makes it worth figuring out manpages...
# - PARALLEL PROCESSING IDEA
#   ~ First, determine number of threads to run based on cores
#   ~ Each thread takes the first element, then deletes it so others can't use it
#   ~ Have a 'lock' variable in place to prevent other threads from grabbing the same element
#     ~ if [[ $lokc -eq 1 ]]; then wait 2s; fi
#   ~ Find a way to isolate the function for each thred so they don't overwrite each other's local vars
#
# v2.1.0, 07 Jan. 2019 16:10 PDT

### Variables

declare -a filePaths # Original paths from the m3u file
declare -a convertedPaths # Paths that have been converted for use with win2UnixPath()
declare -a failedSongs # Songs that fail to convert will be placed here, decide what to do with them later
declare -a allowedFormats # Formats that will be copied instead of converted
m3uFile="" # Self-explanatory
outputFolder="" # I only put a comment here to make it look nice
prefix="" # If the path needs to be changed
w2uMode="" # Change this to 'upper' or 'cut' if needed, see win2UnixPath() documentation for more info
bitrate=128 # Explanatory
preserveLevel="artist" # Artist folder will be saved by default. Also works with album, or none
#ffmpegOptions="" # Random options to be thrown in 
timeoutVal="120s" # Time to wait before assuming a conversion has failed
numberDelimiter=' ' # Defaults to a space, but can be changed by user if needed
total=0 # Total number of songs
songsConverted=0
outputFormat="mp3" # Default format that audio will be converted to
timeBetweenUpdates=60 # Time between progress updates, in seconds
export longName="m3uToUSB" # Used for logging
export shortName="m2u"
export aChar="&"

### Functions

if [[ -f commonFunctions.sh ]]; then
	source commonFunctions.sh
elif [[ -f /usr/share/commonFunctions.sh ]]; then
	source /usr/share/commonFunctions.sh
else
	echo "commonFunctions.sh could not be located!"

	# Comment/uncomment below depending on if script actually uses common functions
	echo "Script will now exit, please put file in same directory as script, or link to /usr/share!"
	exit 1
fi

# convertSong "inputFile" "outputFile"
# Function does NOT add prefixes, edit titles, etc... Only handle conversion using timeout!
function convertSong() {
	# Check if the bitrate has a 'k' in it for functionality
	if [[ "$bitrate" != *k ]]; then
		bitrate="$bitrate""k"
	fi
	
	# Complain if there are not enough arguments
	if [[ "$#" -ne 2 ]]; then
		if [[ -z $1 ]]; then
			debug "l2" "FATAL: No arguments present for convertSong()! Returning..."
			return
		elif [[ -z $2 ]]; then
			debug "l2" "FATAL: Only one argument given, $1 ! Returning..."
			return
		else
			debug "l2" "ERROR: More than two arguments given!"
			debug "l2" "Attempting to run with 1 = $1, 2 = $2"
		fi
	fi
	inputFile="$1"
	outputFile="$2"
	# Check to see if file exists already; delta conversion
	if [[ -f "$outputFile" ]]; then
		#if [[ "$preserveLevel" == "none" && ! -z $overwrite ]]; then
		#	artistFolder="$(echo "$inputFile" | rev | cut -d'/' -f3 | rev)"
		#	fileName="$(echo "$outputFile" | rev | cut -d'/' -f1 | rev)"
		#	container="$(echo "$outputFile" | rev | cut -d'/' -f1 --complement | rev)"
		#	outputFile="$(echo "$container"/"$artistFolder"" - ""$fileName")" # Adds artist name to song title, along with ' - ' in between
		#else
			debug "l5" "File $outputFile already exists! Skipping..." # Was originally "l1", but changed to l5 because log was WAY too large after each run
			return 0
		#fi
	fi
	
	# Warn user of unconvertible files
	if [[ "$inputFile" == *m4p ]]; then
		debug "l2" "WARNING: File $inputFile contains DRM! This file cannot be converted an will be copied instead!"
		cp "$inputFile" "$outputFile"
		return $?
	fi
	
	# If song is already MP3, copy instead of trying to convert
	if [[ "$inputFile" == *mp3 ]]; then
		debug "l5" "$inputFile is an MP3, copying instead of converting"
		cp "$inputFile" "$outputFile"
		return $?
	fi
	
	debug "l5" "Converting $inputFile to $outputFile"
	#timeout --foreground -k "$timeoutVal" 
	if [[ -z $ffmpegOptions ]]; then
		ffmpeg -i "$inputFile" -codec:a libmp3lame -b:a "$bitrate" -id3v2_version 3 -write_id3v1 1 "$outputFile" &>/dev/null
	else
		ffmpeg "$ffmpegOptions" -i "$inputFile" -codec:a libmp3lame -b:a "$bitrate" -id3v2_version 3 -write_id3v1 1 "$outputFile" &>/dev/null
	fi
	value=$?
	if [[ $value -ne 0 ]]; then
		debug "l1" "An error ocurred while converting $inputFile . Exit status: $value"
	fi
	return "$value"
}

function displayHelp() {
read -d '' helpVar <<"endHelp"

m3uToUSB.sh - A script to convert a text playlist of songs to MP3
NOTE: Files with DRM will be copied instead of converted, and you will be notified

Usage: m3uToUSB.sh [options] <playlist_file.m3u> <output_folder>

Options:
   -h | --help                                   : Display this help message and exit
   -p | --preserve <[A]rtist,al[B]um,[N]one>     : Tells script to preserve the artist, album, or no folders (artist is default)
   -m | --link-mode [central_music_folder]       : Puts all music in a central music folder (../Music by default) and hard links back to "playlist" folder
                                                 : Effectively turns folder into a playlist (Warning! Not supported on all devices!)
   -d | --delete                                 : Delete songs no longer in playlist using the diff
   -o | --output <m4a,mp3>                       : Sets your preferred output format. M4A and MP3 only for now.
   -r | --clean-folder                           : Deleter everything from folder before running, start from scratch
   -b | --bitrate <bitrate>                      : Output bitrate. Default 128 kbps (96, 128, 192, 256, and 320 are common values)
   -a | --allowed-formats <list_of_formats>      : Comma-delimited list of supported output formats (e.g. "mp3,flac,m4a")
                                                 : This allows for more files to be copied over instead of converted, preserving quality
   -e | --edit-path-mode <[U]pper,[C]ut>         : Leaves the Windows root uppercase, or cuts it out (see documentation)
   -f | --prefix <folder_prefix>                 : Adds the prefix to each m3u line if it is a Windows path (/mnt, /media)
   -n | --no-overwrite                           : Disables overwriting of conflicting files
   -x | --no-reconversion                        : Disables converting of higher bitrate to lower bitrate of the same format (on by default)
   -l | --localized [(o)utside,(i)nside]         : Outputs a localized playlist fo the same folder ./ (inside), or the containing folder ../ (outside, default)
   -t | --threads <number_Of_Threads>            : Number of parallel conversions to do. Defaults to $CPU_cores/2; 0 disables threads.
   -c | --clean-numbers [delimiter]              : Deletes anything before delimiter in output file (gets rid of numbers before song titles)
                                                 : Delimiter not required, set to space by default. Besure to encase delimiter in ''!
   -u | --no-convert-flac                        : Special option to copy FLAC audio instead of converting it
   
Not limited to .m3u files, any newline delimited file works as well!
Windows paths will be converted automatically, useful if running on Bash for Windows
Only songs HIGHER than the default bitrate will be converted, it will be copied otherwise.
   The only exception to this is if the original codec is not on the list of suppored codecs.
   Songs will never be upconverted, for obvious reasons!

A common use for Bash on Windows would be:
   m3uToUSB.sh --prefex /mnt music.m3u /dev/sdb1
endHelp
echo "$helpVar"
}

function processArgs() {
	if [[ "$1" == "-h" || "$1" == "--help" ]]; then
		displayHelp
		exit 0
	elif [[ $# -lt 2 ]]; then
		debug "l2" "ERROR: Not enough arguments!"
		displayHelp
		exit 1
	fi
	
	while [[ -z $outputFolder ]];
	do
		arg="$1"
		
		case "$arg" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-p|--preserve)
			case "$2" in
				b*|B*|album|Album)
				preserveLevel="album"
				;;
				a*|A*)
				true # Default, nothing to be done
				;;
				n*|N*)
				preserveLevel="none"
				;;
				*)
				debug "l2" "ERROR: Unknown preserve level: $2 ! Please fix and re-run!"
				displayHelp
				exit 1
				;;
			esac
			shift
			;;
			-a|--allowed-formats)
			if [[ "$2" == -* ]]; then
				debug "l2" "ERROR: No list of allowed formats given with $arg! Please fix and re-run!"
				displayHelp
				exit 1
			fi
			if [[ "$2" == *,* ]]; then
				ct="$(printf "$2" | awk -F',' '{print NF-1}')" # Count of number of commas
				while [[ "$ct" -ge 0 ]]; do
					allowedFormats+=("$(printf "$2" | cut -d',' -f $ct)")
					((ct--))
				done
			else
				# Only one file format to add, unless user did not read instructions
				allowedFormats+=("$2")
			fi
			shift
			;;
			-c|--clean-numbers)
			noNumbers="true"
			if [[ $2 == \'* ]]; then # Starts with a '
				numberDelimiter=$2 # Quotes might break this one, needs testing
				shift
			fi
			;;
			--test-import|--testImport)
			if [[ -z $2 ]]; then
				debug "l2" "ERROR: No file given to test importing!"
				displayHelp
				exit 1
			fi
			m3uFile="$2"
			testImport
			exit 0
			;;
			-d|--delete)
			debug "WARN: User has requested to delete old songs at the end"
			deleteMode="1" # If var is present, run deleteSongs(). This value could technically be anything
			;;
			-n|--no-overwrite)
			overwrite="off"
			debug "User has turned off overwriting filed by default"
			;;
			-b|--bitrate)
			bitrate="$2" # Putting a lot of faith in the user
			debug "User set bitrate to: $bitrate"
			shift
			;;
			-e|--edi*)
			case "$2" in
				u*|U*)
				w2uMode="upper"
				debug "User has indicated to use upper with win2UnixPath()"
				;;
				c*|C*)
				w2uMode="cut"
				debug "User has indicated to use cut with win2UnixPath()"
				;;
				*)
				debug "l2" "ERROR: Invalid option eiven with $arg : $2 ! Please fix and re-run!"
				displayHelp
				exit 1
				;;
			esac
			shift
			;;
			-f|--prefix)
			if [[ ! -d "$2" ]]; then
				debug "l2" "ERROR: $2 is not a valid prefix folder! Please fix and re-run!"
				displayHelp
				exit 1
			fi
			prefix="$2"
			if [[ "$prefix" == */ ]]; then
				prefix="$(echo "$prefix" | rev | cut -d'/' -f 1 --complement | rev)" # Cuts the trailing slash, if present, to prevent errors
			fi
			shift
			;;
			--delete-only)
			export deleteOnly=1
			;;
			-l|--localized)
			debug "l2" "ERROR: Localization is still a work in progress! Should be done soon! Skipping for now..."
			if [[ "$2" != -* ]]; then
				shift
			fi
			;;
			*)
			# Minimum two arguments - .m3u and output folder. Check validity and move on if so
			if [[ -f "$arg" ]]; then
				[[ -z $2 ]] && debug "l2" "ERROR: No output directory given! Please fix and re-run!" && displayHelp && exit 1 # One-liners are the best
				m3uFile="$arg"
				outputFolder="$2" # Assuming it is a directory for now, will be checked later
				return # Slightly messy, but there shouldn't be any arguments after this anyways
			fi
			debug "l2" "ERROR: Unknown option given: $arg ! Please fix and re-run!"
			displayHelp
			exit 1
			;;
		esac
		shift
	done
}

function importM3U() {
	# Import the whole m3u file into an array, since it might need to be used multiple times
	# http://stackoverflow.com/questions/21121562/shell-adding-string-to-an-array
	
	announce "Now preparing files for conversion..." "This process may take a minute depending on CPU power and playlist size"
	dos2unix "$m3uFile"
	
	while read -r line
	do
		filePaths+=("$line")
	done < "${m3uFile}"
	
	# TODO: Redo this whole friggin thing. Adding diff/comm completely messed this up. Hope the tests work...
	# Now that the lines are imported, make sure they are all Linux-friendly
	newFile="$outputFolder""/""$(echo "$m3uFile" | rev | cut -d'/' -f1 | rev)"
	if [[ -f "$newFile" ]]; then
        rm "$newFile" # Disable this once we figure out how to handle file deletion via diff
    fi
	touch "$newFile"
	
	for path in "${filePaths[@]}"
	do
		if [[ "$path" == \#* ]]; then
            echo "$path" >> "$newFile"
		elif [[ -f "$path" ]]; then
			convertedPaths+=("$path") # This should save a lot of time with native Linux m3u files
			((total++))
			echo "$path" >> "$newFile"
		else
			tpath="$(win2UnixPath "$path" "$w2uMode")"
			convertedPaths+=("$tpath")
			((total++))
			echo "$tpath" >> "$newFile"
		fi
	done
	#newFile=".""$(echo "$m3uFile" | rev | cut -d'/' -f1 | rev)"
	
	# Delete old playlist if present
	if [[ -f "$origFile".old ]]; then
		debug "WARN: Deleting old playlist $origFile.old!"
		rm "$origFile".old
	fi
	
	# Now, copy over the "new" old playlist, if present
	if [[ -f "$origFile" ]]; then
		debug "INFO: Copying last playlist $origFile to .old!"
		mv "$origFile" "$origFile".old
	fi
	
	cp "$m3uFile" "$origFile" # Makes a copy of the current playlist, hidden, in the output folder
}

function testImport() {
	#if [[ -z $1 || ! -f "$1" ]]; then
	#	echo "Please run with a file!"
	#	exit 1
	#fi

	#m3uFile="$1"
	importM3U

	echo " "
	echo "Showing contents of filePaths..."
	pause
	for item in "${filePaths[@]}"
	do
		echo "$item"
	done

	pause

	echo " "
	echo "Showing contents of convertedPaths..."
	pause
	for items in "${convertedPaths[@]}"
	do
		echo "$items"
		printf "Is it a file? "
		if [[ -f "$items" ]]; then
			printf "True\n"
		else
			printf "False\n"
		fi
	done
	exit 0
}

function touchTest() {
	touch "$1"
	estatus=$?
	case "$estatus" in
		0)
		debug "Folder $1 passed the touch test!"
		;;
		*)
		debug "l2" "ERROR: You do not have write permission for the folder $1 ! Please fix, or select a different folder, and re-run!"
		displayHelp
		exit $estatus
		;;
	esac
}

# outputFilename "input file"
# Everything else is taken from global variables, which should already be set
function outputFilename() {
	# Just to be sure...
	[[ -z $1 ]] && debug "ERROR: No argument supplied to outputFilename!" && return
	
	# First, we create the variables. Then, we use them based on user decision
	artistFolder="$(echo "$1" | rev | cut -d'/' -f3 | rev)"
	albumFolder="$(echo "$1" | rev | cut -d'/' -f2 | rev)"
	fileName="$(echo "$1" | rev | cut -d'/' -f1 | cut -d'.' -f1 --complement | rev)" # Wasted cycles, but who cares with today's processors?
	[[ ! -z $noNumbers ]] && [[ "$fileName" == [0-9]* ]] && fileName="$(echo "$fileName" | cut -d "$numberDelimiter" -f1 --complement)" # I was gonna save this for a later date, but the implementation was simple
	
	case "$preserveLevel" in
		none)
		newFile="$outputFolder"/"$fileName"
		# This loop will handle multiple copies of similar titled songs (e.g allows Monster by Starset and Monster by Skillet in the same folder
		# Really only applies to non-artist/album folders
		count="$(printf "%s\n" "${convertedPaths[@]}" | grep -i "$fileName" | wc -l)" # Using -i because Windows is case-insensitive
		if [[ "$count" -gt 1 ]]; then
            newFile="$newFile"-"$count"".mp3"
        else
            newFile="$newFile"".mp3"
        fi
		;;
		artist)
		newFile="$outputFolder"/"$artistFolder"
		if [[ ! -d "$newFile" ]]; then
			mkdir "$newFile"
			#[[ "$#" -eq 0 ]] || debug "l2" "ERROR: Unable to create folder: $newFile ! Please fix and re-run!" # Simple error checking
			folderTest "$newFile"
		fi
		newFile="$newFile"/"$fileName"".mp3"
		;;
		album)
		# Artist folder first
		newFile="$outputFolder"/"$artistFolder"
		if [[ ! -d "$newFile" ]]; then
			mkdir "$newFile"
			folderTest "$newFile"
		fi
		
		# Now, check album folder
		newFile="$newFile"/"$albumFolder"
		if [[ ! -d "$newFile" ]]; then
			mkdir "$newFile"
			folderTest "$newFile"
		fi
		newFile="$newFile"/"$fileName"".mp3"
		;;
		*)
		debug "l2" "A fatal error has occurred! Unknown preserve level: $preserveLevel!"
		exit 1
		;;
	esac
	
	# Now that that's all over with and output file is ready with working directories
	echo "$newFile"
}

function converterLoop() {
	announce "Beginnning conversion progress!" "This will take a while depending on processor speed and playlist length." "This screen will only show errors, but will notify you when it is complete."
	sleep 3
	displayProgress # Initialize it, makes log look better and gets a better "start time"
	
	shopt -s nocasematch
	shopt -s nocaseglob
	for songFile in "${convertedPaths[@]}"
	do
		if [[ ! -f "$songFile" ]]; then
			#echo "songFile: $songFile"
			debug "l2" "WARNING: Source file not found: $songFile"
			failedSongs+=("$songFile") # Place files not found in this array
			continue
		fi
		
		currentFile="$(outputFilename "$songFile")"
		convertedPaths=("${convertedPaths[@]/$songFile}") # Comment THIS line if things start failing. Deletes array elements after input processing. Effectively FIFO.
		# If anyone ever asks why I love functions, I will show them this. 
		# The function right here is the reason I love programming (or scripting, to be more specific)
		convertSong "$songFile" "$currentFile"
		if fileTest "$currentFile" # Only really reports the error... Better logging this way
		then
			((songsConverted++))
		fi
		displayProgress # Once again, on today's processors, this means nothing
	done
}

function folderTest() {
	if [[ ! -d "$1" ]]; then
		debug "l2" "$1 is not a directory! mkdir failed, or insufficient permissions!"
		return 1
	fi
	return 0
}

function fileTest() {
	if [[ ! -f "$1" ]]; then
		debug "l2" "WARNING: File $1 could not be found, conversion failure!"
		return 1
	fi
	return 0
}

function outputFailures() {
	printf "\n\nFailed songs from playlist %s:\n" "$(echo "$m3uFile" | rev | cut -d'/' -f1 | rev)" >> failedSongs.txt
	for failure in "${failedSongs[@]}"
	do
		echo "$failure" >> failedSongs.txt
	done
	debug "l2" "List of failed songs has been output to $(pwd)/failedSongs.txt at user request!"
}

function determinePath() {
	# Now, try to 'find' these files
	artistFolder="$(echo "$1" | rev | cut -d'/' -f3 | rev)"
	albumFolder="$(echo "$1" | rev | cut -d'/' -f2 | rev)"
	fileName="$(echo "$1" | rev | cut -d'/' -f1 | rev)"
	container="$(echo "$1" | rev | cut -d'/' -f1-3 --complement | rev)"
	extension="$(echo "$fileName" | rev | cut -d'.' -f1 | rev)" # the rev's in this one make sure periods in filename don't get cut
	fileName="$(echo "$fileName" | rev | cut -d'.' -f1 --complement | rev)" # Same reason as above
	
	# First, check container
	if [[ -d "$container" ]]; then
		true
	else
		debug "l2" "ERROR: Bad folder container: $container"
		return 1
	fi
		
	# Artist folder
	testPath="$container"/"$artistFolder"
	if [[ -d "$testPath" ]]; then
		true
	else
		# Problem with the artist folder (rare)
		# Now see if running through 'title' makes it work
		artistFolder="$(echo -e "$artistFolder" | sed -r 's/\<./\U&/g')"
		testPath="$container"/"$artistFolder"
		if [[ -d "$testPath" ]]; then
			true
		else
			# No cutting the name here, might cause errors
			testPath="$(find "$container" -iname "$artistFolder*" -print0)"
			if [[ -d "$testPath" ]]; then
				artistFolder="$(echo "$testPath" | rev | cut -d'/' -f1 | rev)"
			else
				debug "l2" "ERROR: Could not locate artist folder: $artistFolder !"
				return 1
			fi
		fi
	fi
	
	# Album folder
	testPath="$container"/"$artistFolder"/"$albumFolder"
	if [[ -d "$testPath" ]]; then
		true
	else
		# Problem with album folder (most common)
		# Now see if running through 'title' makes it work
		albumFolder="$(echo -e "$albumFolder" | sed -r 's/\<./\U&/g')"
		testPath="$container"/"$artistFolder"/"$albumFolder"
		if [[ -d "$testPath" ]]; then
			# Worked, now see if fixed album+fileName works
			testPath="$testPath"/"$fileName"."$extension"
			if [[ -f "$testPath" ]]; then
				echo "$testPath"
				return 0
			else
				false # Now folder is present, but filename may be wrong
			fi
		else
			# Folder still not found, multiple errors maybe?
			# See if 'find' can find folder
			testPath="$(find "$container/$artistFolder" -iname "$(echo "$albumFolder" | cut -d' ' -f1)*" -print0)"
			if [[ -d "$testPath" ]]; then
				# Find was able to find the folder in question
				albumFolder="$(echo "$testPath" | rev | cut -d'/' -f1 | rev)"
				testPath="$container"/"$artistFolder"/"$albumFolder"/"$fileName"."$extension"
				if [[ -f "$testPath" ]]; then
					# File was found in 'found' folder!
					echo "$testPath"
					return 0
				else
					false # Still an issue with filename, directory is good though so move on
				fi
			else
				debug "l2" "ERROR: Album folder $albumFolder could not be found in parent $artistFolder !"
				return 1
			fi
		fi
	fi
	
	# If you made it this far, only option left is bad filename
	# First, attempt to fix case (usually the issue)
	fileName="$(echo -e "$fileName" | sed -r 's/\<./\U&/g')"
	testPath="$container"/"$artistFolder"/"$albumFolder"/"$fileName"."$extension"
	if [[ -f "$testPath" ]]; then
		echo "$testPath"
		return 0
	else
		# Fixing case of file did not work, now try to find based on first word or number
		testPath="$(find "$container/$artistFolder/$albumFolder" -iname "$(echo "$fileName" | cut -d' ' -f1)*" -print0)"
		if [[ -f "$testPath" ]]; then
			# Hope you got the right file!
			echo "$testPath"
			return 0
		else
			# File could not be found anywhere
			debug "l2" "FATAL: File could not be located: $testPath , giving up on it!"
			return 1
		fi
	fi
}

function processFailures() {
	announce "Now attempting to process files that gave errors during conversion!" "Most of these just need their paths fixed, but other failures will be reported."
	sleep 3
	
	declare -a failedList
	failures=0
	fixedFailures=0
	for song in "${failedSongs[@]}"
	do
		failedList+=("$(determinePath "$song")")
		((failures++))
	done
	
	for songFile in "${failedList[@]}"
	do
		if [[ "$songFile" == *ERROR:* ]]; then
			continue # Just in case something got through
		fi
		
		currentFile="$(outputFilename "$songFile")"
		# If anyone ever asks why I love functions, I will show them this. 
		# The function right here is the reason I love programming (or scripting, to be more specific)
		convertSong "$songFile" "$currentFile"
		if fileTest "$currentFile" # Only really reports the error... Better logging this way
		then
			((fixedFailures++))
		fi
	done
}

function displayProgress() {
	if [[ -z $firstUpdate ]]; then
		printf "Script is now working on converting %s songs and will display updates every ~%s seconds\n" "$total" "$timeBetweenUpdates"
		firstUpdate="done"
		lastUpdate="$SECONDS"
		startTime="$SECONDS"
		oldSongsConverted=0
		return
	fi
	
	currentTime="$SECONDS"
	if [[ $(( currentTime - lastUpdate )) -ge "$timeBetweenUpdates" ]]; then
		printf "[%s/%s] Songs converted so far. %s songs processed in the past %s seconds.\n" "$songsConverted" "$total" "$(( songsConverted - oldSongsConverted ))" "$(( currentTime - lastUpdate ))"
		oldSongsConverted="$songsConverted"
		lastUpdate="$currentTime"
	fi
}

function deleteSongs() {
	fileA="$origFile"
	fileB="$origFile".old # Easier this way
	pDiff="$fileA".diff
	
	# Make sure a diff will be successful
	if [[ ! -f "$fileB" ]]; then
		debug "l2" "FATAL: Unable to delete songs because old playlist is not present! Please manually delete songs!"
		return 1
	fi
	
	# Now, DELETE.
	# This next part courtesy of StackOverflow
	# https://serverfault.com/questions/68684/how-can-i-get-diff-to-show-only-added-and-deleted-lines-if-diff-cant-do-it-wh
	sort <"$fileA" >a.sorted
	sort <"$fileB" >b.sorted
	sed -i '/^#/ d' a.sorted b.sorted # Gets rid of all comments lines so only files are shown
	comm -13 a.sorted b.sorted >"$pDiff"
	
	if [[ "$(cat "$pDiff" | wc -l)" -eq 0 ]]; then # File is empty, no newlines. Even if it weren't empty, its inoperable
		debug "l2" "INFO: No songs were deleted since last update, returning successfully!"
		return 0
	else
		local tPath
		while read -r line
		do
			if [[ -f "$line" ]]; then
				tPath="$(outputFilename "$line")"
			else
				tPath="$(outputFilename "$(win2UnixPath "$line" "$w2uMode")")" # VERY experimental. Wouldn't be suprised if this throws an error in shellcheck or during testing
			fi
			
			if [[ ! -f "$tPath" ]]; then
				debug "l2" "ERROR: File $tPath was marked for deletion, but does not exist!" # This was the stop point. Look here if things go wrong 
			else
				debug "l5" "WARN: Deleting song $tPath..."
				rm "$tPath" # TODO: Double check that full filenames are used here if errors
			fi
		done <"$pDiff"
	fi
	rm a.sorted b.sorted "$pDiff"
	debug "INFO: Done deleting files!"
}

### Main Script

processArgs "$@"
#testImport # This line is used for debugging. You can also use the secret --test-import option to do this
checkRequirements "ffmpeg" "dos2unix" "comm/coreutils" "mediainfo" #"parallel" #"libmp3lame0"
if [[ -z $overwrite ]]; then 
	# Warn user about accidentally overwriting files with -c set without -n
	if [[ "$noNumbers" == "true" && "$preserveLevel" == "none" ]]; then
		announce "WARNING: It is advised to run -c with -n when preserve level is set to none" "Otherwise, similar song titles might be overwritten" "Press CTRL+C now to fix this. Script will continue shortly."
	fi
	
	if [[ -z $ffmpegOptions ]]; then
		ffmpegOptions="-y"
	else
		ffmpegOptions="$ffmpegOptions""-y"
	fi
fi

# Error checking for outputFolder should only trigger if it is not a valid directory
if [[ ! -d "$outputFolder" ]]; then
	debug "l2" "ERROR: $outputFolder is not a directory!"
	getUserAnswer "n" "Would you like to attempt to make this directory? (Be careful!)"
	case $? in
		0)
		debug "INFO: Attempting to create directory..."
		mkdir "$outputFolder"
		folderTest "$outputFolder"
		value=$?
		case $value in
			0)
			debug "l3" "Folder $outputFolder created successfully! Moving on..."
			;;
			*)
			debug "l2" "FATAL: Error while attemping to create folder: exit status $value"
			announce "Please fix the error and re-run the script!"
			displayHelp
			exit $value
			;;
		esac
		;;
		1)
		debug "WARN: User decided not to make new folder, exiting script..."
		announce "Please find another directory and re-run the script!"
		displayHelp
		exit 1
		;;
		*)
		debug "l2" "ERROR: Unknown exit status!"
		displayHelp
		exit 1
		;;
	esac
fi
touchTest "$outputFolder"
if [[ "$outputFolder" == */ ]]; then # If it made it this far, folder is ready for use. Cut trailing slash if present
	outputFolder="$(echo "$outputFolder" | rev | cut -d'/' -f 1 --complement | rev)"
fi

# Ready to start converting. Import files, then loop!
origFile="$outputFolder"/".""$(echo "$m3uFile" | rev | cut -d'/' -f1 | rev)"".orig" # Easier to set here. Used for comm/diff and file deletion
importM3U

# Debugging
if [[ ! -z $deleteOnly ]]; then
	debug "l2" "WARN: Attempting to delete songs and exit!"
	deleteSongs
	debug "l2" "INFO: Done deleting songs, function returned $?"
	exit 0
fi

converterLoop # Convert everything
#outputFailures # More debugging

if [[ "${#failedSongs[@]}" -ne 0 ]]; then
	debug "l2" "INFO: Attempting to convert failed songs..."
	processFailures
fi

# Finally, delete songs if requested
if [[ ! -z $deleteMode ]]; then
	debug "WARN: Deleting songs using diff/comm!"
	deleteSongs
fi

announce "Script has completed successfully!" "Please consult log for any file that could not be converted!"
debug "INFO: Total songs: $total, converted songs: $songsConverted, failed songs: $failures, fixed songs: $fixedFailures"

#EOF
