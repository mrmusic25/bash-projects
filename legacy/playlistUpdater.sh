#!/bin/bash
#
# playlistUpdater.sh - An all-in-one update tool for converting all the iTunes playlists
#
# Requirements: itunesExporter.py - Exports all playlists from iTunes Library.xml to .m3u8 files
#                                   Found at https://gitlab.com/mrmusic25/python-projects
#               m3uToUSB.sh       - Script to convert a playlist to a folder
#                                   Found in same repo as this script, https://gitlab.com/mrmusic25/bash-projects
#               libpytunes (pip)  - Used by python to process iTunes Library.xml (sudo pip install libpytunes)
#
# Usage: ./playlistUpdater.sh [setup] - Everything is automated, unless setup is run either by user request or first-time setup
#
# Changes:
# v2.1.4
# - Figured out once and for all the way to use sed!
# - Now, any non-allowed characters (e.g. emoji) will simply be 'x'
# - Disabled sha256sum until I have time to fully implement planned function
#
# v2.1.3
# - Fixed issues with sed
# - Placeholder pause() til I can figure out why script moves on without input
#
# v2.1.2
# - Added another sed edit for common playlist characters
# - Script will now ask user to link to /usr/bin if desired
# - Fixed critical bug in which I used the wrong variable name, once again
#
# v2.1.1
# - Tested and ready for release!
# - Extra options and TODOs will be fixed later, but for now it works!
#
# v2.1.0
# - All of the code is done, just needs to be tested for release
# 
# v2.0.0
# - Initial rewrite of script
#
# TODO:
# - Make script grab "missing pieces" to make it fully self-supported
#   ~ e.g. Only have to hand this script to people, does everything else on its own
#   ~ curl commonFunctions, pmCF, python script, and m3uToUSB
# - Add the rest of the m3uToUSB options instead of them just being global
# - Add localization.sh to this repo
#   ~ Have the script called to localize the playlists
#   ~ Another possible var - where localized playlists should be output? Would require change to m2u
# - Implement SHA256 check
#   ~ See if playlist should even be converted
#   ~ Remember to use dos2unix before checking sum!
#
# v2.1.4, 07 Jan. 2019, 16:17 PDT


### Prep 

export shortName="pu"
export longName="playlistUpdater"
source /usr/share/commonFunctions.sh
export aChar="\\" # Experimenting with different looks, I like this one

### Variables

numThreads=1 # Number of simultaneous playlists to be run at the same time
runThreads=0 # Current running threads

### Functions

# Cycles through all given args to see if they are set. Returns 1 if any are unset, 0 if all variables are set
function isUnset() {
	local i=1
	local num="$#"
	while [[ $i -le $num ]]; do
		local var=${!i}
		if [[ -z ${!var} || "${!var}" == "" ]]; then
			debug "l2" "ERROR: ${var} is not set! DBG: ${!var}"
			return 1
		else
			debug "INFO: ${var} is set to ${!var}"
		fi
		((i++)) # Almost created an infinite loop there lol
	done
	debug "INFO: All variables given are set properly!"
	return 0
}

# 2 args required: $1 - Name of title box, $2 - description of item to find
# Function could technically without them, but it's better to give the user context
function getLocation() {
	if [[ -z $1 || -z $2 ]]; then
		debug "l2" "ERROR: Not enough args given for getLocation()! 1=$1, 2=$2"
		echo ""
		#return 1
	fi
	local val="$(dialog --title "$1" --stdout --title "$2" --fselect "$(pwd)" 14 48)"
	echo "$val"
	#return 0
}

# Requires no args. Asks user to set everything up using the dialog package
# TODO: Check to see if var is already set (create another function? Use isUnset()? Figure out use cases)
function setup() {
	debug "INFO: Beginning setup!"
	announce "The following screens will help you setup this script for automation" "Everything can be changed later, see documentation!" "Follow the prompt at each screen!"
	
	debug "WARN: Deleting old config to reduce errors"
	myConfig "remove"
	
	# Collect file information
	myConfig comment "Below are the locations of the necessary scripts and folders for this all to work"
	while [[ -z $itunesExporterLocation || ! -f "$itunesExporterLocation" ]]; do
		export itunesExporterLocation="$(getLocation "Locate itunesExporter.py" "Enter location for itunesExporter.py")"
	done
	myConfig add "itunesExporterLocation"
	debug "l2" "INFO: Making sure libpytunes is installed with pip, sudo privilege required!"
	sudo pip install --upgrade libpytunes
	
	# iTunes XML
	while [[ -z $itunesXMLLocation || ! -f "$itunesXMLLocation" ]]; do
		export itunesXMLLocation="$(getLocation "Locate iTunes Library.xml" "Enter location for iTunes Library.xml")"
	done
	myConfig add "itunesXMLLocation"
	
	# m3uToUSB.sh
	if [[ ! -z $(which m2u) ]]; then
		debug "INFO: Using linked location for m3uToUSB.sh!"
		export m2uLocation="$(which m2u)"
	elif [[ -f "m3uToUSB.sh" ]]; then
		debug "WARN: Using same folder for m3uToUSB.sh location, script must be run in same folder!"
		export m2uLocation="m3uToUSB.sh"
	else
		while [[ -z $m2uLocation || ! -f $m2uLocation ]]; do
			export m2uLocation="$(getLocation "Locate m3uToUSB.sh" "Locate the M3U conversion script (m3uToUSB.sh)")"
		done
	fi
	myConfig add "m2uLocation"
	
	# Warn user about creating directories beforehand
	announce "Folders for M3U8 playlists and the output folder must exist!" "If they do not exist already, please go create the directories now!"
	pause "Press CTRL+C to create directories in terminal, or press [Enter] when you have created them otherwise: "
	
	# M3U output directory from itunesExporter.xml
	while [[ -z $m3uFolderLocation || ! -d "$m3uFolderLocation" ]]; do
		export m3uFolderLocation="$(getLocation "Locate the M3U8 folder" "Select the folder where the M3U8 files will be exported")"
	done
	myConfig add "m3uFolderLocation"
	
	# Final export location for processed music playlists
	while [[ -z $m3uOutputLocation || ! -d "$m3uOutputLocation" ]]; do
		export m3uOutputLocation="$(getLocation "Locate the music output folder" "Selecte the folder where the music will be exported")"
	done
	myConfig add "m3uOutputLocation" # FML
	
	# Export the default settings to be used when a new script is being converted
	myConfig comment "Below are the default settings used for each script. Can be changed by user."
	export defaultPreserve="n" # No preservation, used in all but a couple playlists generally
	myConfig add "defaultPreserve"
	
	export defaultClean="y" # Whether or not to clean numbers. Generally on ('0')
	myConfig add "defaultClean"
	
	export defaultOverwrite="y" # Disables overwriting of songs with the same name. Highly encouraged when using clean numbers, so both are on by default
	myConfig add "defaultOverwrite"
	
	if getUserAnswer "Do you need to set a global prefix for your music location? (See documentation of m3uToUSB)"; then
		while [[ -z $defaultPrefix || ! -d "$defaultPrefix" ]]; do 
			export defaultPrefix="$(getLocation "Locate the prefix" "Select the folder prefix for your music")"
		done
		myConfig add "defaultPrefix"
	fi
	
	# Notify user that their suffering is over
	announce "All automated variables set now, saving config and moving on!"
	myConfig "comment" "And now, below are all the settings for the playlists! Stored based on sha256sum | head -c8"
	myConfig "export"
}

# Only one input needed - playlist name. Everything else is gloabl, or will be generated
# Saved playlist name is based off the first 8 bytes of the sha256sum, so case matters! You will have to manually delete changed playlists because of this!
function convertPlaylist() {
	# Sanity check
	if [[ -z $1 ]]; then
		debug "l2" "FATAL: No playlist given to convertPlaylist()! Please fix and re-run!"
		return 1
	elif [[ ! -f $1 ]]; then
		debug "l2" "ERROR: File $1 is not a text file, cannot be used! Please fix and re-run!"
		return 1
	fi
	
	# Now, for the multitude of variables needed
	export pName="$(printf "%s" "$1" | rev | cut -d'/' -f1 | rev)" # Name of playlist
	#playlistSHA="$(printf "%s" "$1" | sha256sum | cut -d' ' -f1)" # Used for comparison, see if conversion is even needed
	export sName="$(printf "%s" "$pName" | cut  -d'.' -f1 | sed -e "s|[ -]|_|g" -e "s|[();,.:'\"/-\!\?\\]||g" -e "s/[^a-zA-Z0-9_]/x/g")" # Sometimes I hate bash. Figured out why it wouldn't work, but this is more readable anyways
	outFolder="$m3uOutputLocation"/"$sName" # Sets the output folder
	
	# This is a specific for the script creator only. Everyone else can comment this out the next 4 lines, if so desired
	if [[ "$pName" == *Album* ]]; then
		debug "l5" "DBG: Hello, master!"
		outFolder="$m3uOutputLocation"
	fi
	#debug "l5" "DBG: sName=$sName, outFolder=$outFolder, pName=$pName" # Debug purposes
	
	# NOTE: This CANNOT be parallel-ized until this is a local! Might have to get rid of expansion in myConfig() again...
	export tryVar="plist_""$sName""_convert" # Var name that let's script know if playlist has existed before, and whether or not to convert
	#declare ${!tryVar}
	if [[ -z ${!tryVar} ]]; then
		debug "INFO: Playlist $pName has not been converted before! Asking user for guidance."
		if getUserAnswer "$pName has not been converted before, would you like to convert it?"; then
			debug "l5" "DBG: Enabling conversion of $pName at user request"
			declare ${tryVar}="y" # Can't believe this actually worked. Theory-tested on WSL, Bash 4.4.19(1)-release
			myConfig add "$tryVar"
		else
			debug "l5" "DBG: Disabling conversion of $pName!"
			declare ${tryVar}="n"
			myConfig add "$tryVar"
			return 0 # Technically a success
		fi
		pause "Placeholder until maintainer can diagnose problem; press [Enter] to continue: "
	fi
	
	# I made the word y/n for a reason, so I can use getUserAnswer() to decide how to proceed! Efficency!
	if ! getUserAnswer "${!tryVar}" "Would you like to convert playlist $pName?"; then
		debug "INFO: Playlist $pName will not be converted!"
		declare ${tryVar}="n"
		myConfig add "$tryVar"
		return 0
	else
		debug "INFO: Continuing with conversion of $pName!"
		declare ${tryVar}="y"
		myConfig add "$tryVar"
	fi
	
	# Assuming you want to convert at this point. Making sure variables are set
	export tryPreserve="$sName""_preserve"
	export tryClean="$sName""_clean"
	export tryOverwrite="$sName""_overwrite"
	
	if [[ -z ${!tryPreserve} && -z ${!tryClean} && -z ${!tryOverwrite} ]]; then # All variables are missing, set to default and confirm with user
		debug "l2" "WARN: No conversion settings found for playlist $pName! Setting to defaults"
		declare ${tryPreserve}="$defaultPreserve"
		declare ${tryClean}="$defaultClean"
		declare ${tryOverwrite}="$defaultOverwrite"
		
		announce "Please confirm the settings for $pName in the following screens"
		
		# First, confirm preserve level
		declare ${tryPreserve}="$(dialog --clear --stdout --radiolist \
		"Choose a preserve level. Input ../Music/Artist/Album/01 Song.aac becomes:" 12 80 3 \
		n "[N]one      ../Playlist/Song.mp3" on \
		a "[A]rtist    ../Playlist/Artist/Song.mp3" off \
		b "Al[b]um     ../Playlist/Artist/Album/Song.mp3" off)" # Feels dirty coding like this, but it's unreadable otherwise.
		
		# Next, ask to clean numbers
		declare ${tryClean}="$(dialog --clear --stdout --radiolist \
		"Would you like to clean numbers? Input ../01 Song.aac becomes:" 12 80 2 \
		y "Yes     ../Song.mp3" on \
		n "No      ../01 Song.mp3" off)"
		
		# Finally, recommend overwriting
		declare ${tryOverwrite}="$(dialog --clear --stdout --radiolist \
		"Disable overwriting of same song titles? HIGHLY recommended if clean numbers is on! Given ../01 Monster.aac and ../07 Monster.aac" 12 80 2 \
		y "Yes    Monster.mp3, Monster-1.mp3 (both songs)" on \
		n "No     Monster.mp3 (only the second song)" off)"
		
		debug "INFO: Settings confirmed for $pName, exporting now!"
		myConfig add "${tryPreserve}"
		myConfig add "${tryClean}"
		myConfig add "${tryOverwrite}"
		myConfig "export" # Can't hurt, mostly for debugging
	fi
	
	# Now, setup the eval statement
	# TODO: Add globals here when ready
	if ! mkdir -p "$outFolder"; then
		debug "FATAL: Could not create folder $outFolder! Illegal characters, or no permissions?"
		return 1
	fi
	
	local eStatement="$m2uLocation -p ${!tryPreserve}"
	
	# Now for the if statements... Doubt there's a better way to do this
	if [[ ! -z $defaultPrefix ]]; then
		eStatement="$eStatement"" -f $defaultPrefix"
	fi
	
	if [[ "${!tryClean}" == "y" ]]; then
		eStatement="$eStatement"" -c"
	fi
	
	if [[ "${!tryOverwrite}" == "y" ]]; then
		eStatement="$eStatement"" -n"
	fi
	
	#eStatement="$eStatement $1 $outFolder"
	
	# Finally, run and report any errors
	debug "INFO: Running the following command: $eStatement $(printf "%s" "$1" | sed -e 's/ /\\ /g') $outFolder"
	eval "$eStatement" "$(printf "%s" "$1" | sed -e 's/ /\\ /g')" "$outFolder"
	local rval="$?"
	if [[ "$rval" -ne 0 ]]; then
		debug "l2" "ERROR: Script ran unsuccessfully for $pName with error code $rval!"
	else
		debug "l2" "INFO: Script ran successfully for $pName!"
	fi
	
	return "$rval"
}

### Main script
checkRequirements "python" "dialog" "tee" "pip/python-pip" # Not checking for ffmpeg because m2u does that already

# Changed the order so this is first, prevents overwriting of new vars
if [[ "$1" == "setup" ]]; then
	debug "l2" "WARN: Running setup at user's request!"
	setup
fi

myConfig "import" # Import the config and attempt to run
if ! isUnset "itunesExporterLocation" "itunesXMLLocation" "m2uLocation" "m3uFolderLocation" "m3uOutputLocation"; then
	debug "l2" "WARN: Some settings could not be found, entering setup!"
	setup
fi

# Assuming 'threads' and setup will never be run at the same time
if [[ "$1" == *-t* ]]; then
	if [[ "$2" -ne "$2" ]]; then
		debug "l2" "ERROR: No number of threads was given! Please fix and re-run!"
		exit 1
	fi
	numThreads="$2" # TODO: Add checking of threads vs cores available, warn user about speed
fi

# Ask user to link to /usr/bin, if desired
if [[ ! -e /usr/bin/pu ]]; then
	announce "Would you like to link this script to /usr/bin?" "This allows you to run the command 'pu' from anywhere to run automated script"
	if getUserAnswer "n" "Would you like to link the script now? (Requires sudo privilege)"; then
		debug "INFO: Linking script to /usr/bin!"
		dynamicLinker "$(pwd)/$(printf "%s" "$0" | cut -d'/' -f2)"
	fi #else, do nothing
fi

# First step, output all the playlists
debug "INFO: Exporting playlists to $m3uFolderLocation!"
python "$itunesExporterLocation" "$itunesXMLLocation" "$m3uFolderLocation"
rtval="$?"
if [[ "$rtval" -ne 0 ]]; then
	debug "ERROR: An error occurred while running $itunesExporterLocation! Code: $rtval Terminating script..."
	exit 1
fi

# Now, switch to directory and process all .m3u8 files
OPWD="$(pwd)"
cd "$m3uFolderLocation"
for playlist in *.m3u*; do
	#while [[ "$runThreads" -ge "$numThreads" ]]; 
	#do
	#	sleep "$sTime"
	#done
	#((runThreads++))
	convertPlaylist "$(pwd)"/"$playlist"
	#rtnval="$?"
	#if [[ "$rtnval" -ne 0 ]]; then
	#	debug "l2" "ERROR: $playlist could not be converted! See log for more details!"
	#fi
done

debug "l3" "INFO: Done converting all playlists in the folder! Cleaning up and exiting now..."

cd "$OPWD"
myConfig "export" # Make sure all settings are saved at the end
#EOF