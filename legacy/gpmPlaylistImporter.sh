#!/bin/bash
#
# gpmPlaylistConverter.sh - Quick n dirty script purpose built to read the input from the following python script
# https://gitlab.com/mrmusic25/python-projects/-/blob/master/getPlaylists.py

source /usr/share/commonFunctions.sh
export debugLevel=1

musicIndex="/mnt/f/musicIndex.txt"
musicLibrary="/mnt/f/iTunes/iTunes Media/Music"
playlistFolder="/mnt/f/temp"
manuals="/mnt/f/manuals.txt"

function clean() {
    echo "$1" | sed -e 's/[^a-zA-Z0-9,._+@%/-]/\\&/g; 1{$s/^$/""/}; 1!s/^/"/; $!s/$/"/'
}

### Main 

# Remove old music library and re-scan folder
if [[ -f "$musicIndex" ]]; then
    debug "l2" "Deleting old music index at $musicIndex"
    rm "$musicIndex"
fi
indexFolder "$musicLibrary" "$musicIndex"

# Remove old manuals file if it exists
if [[ -f "$manuals" ]]; then
    debug "l2" "Removing old manuals file..."
    rm "$manuals"
fi

OPWD="$(pwd)"
cd "$playlistFolder"

# Now, process each text folder
for file in *.txt; do
    debug "l1" "Working on playlist $file..."

    artist=""
    album=""
    title=""
    pFile="$(echo "$file" | rev | cut --complement -d'.' -f1 | rev)".m3u
    if [[ -f "$pFile" ]]; then
        debug "l2" "Removing old playlist $pFile"
        rm "$pFile"
    fi

    touch "$pFile"
    
    while read -r line; do
        artist="$(printf "%s" "$line" | cut -d'|' -f1)"
        album="$(printf "%s" "$line" | cut -d'|' -f2)"
        title="$(printf "%s" "$line" | cut -d'|' -f3)"

        grep "$title" "$musicIndex" > tmp.txt 2>/dev/null
        if [[ $(wc -l tmp.txt | cut -d' ' -f1) -gt 1 ]]; then
            grep "$album" tmp.txt > tmp.txt 2>/dev/null
            if [[ $(wc -l tmp.txt | cut -d' ' -f1) -gt 1 ]]; then
                grep "$artist" tmp.txt > tmp.txt 2>/dev/null
                #if [[ $(wc -l tmp.txt | cut -d' ' -f1) -ne 1 ]]; then
                #    grep "$album" "$musicIndex" > tmp.txt 2>/dev/null
                #    grep "$title" tmp.txt > tmp.txt 2>/dev/null
                #fi
            fi
        fi
        
        if [[ $(wc -l tmp.txt | cut -d' ' -f1 ) -gt 1 ]]; then
            debug "l3" "Found multiple matching files for $line in index!"
            cat tmp.txt
            pause "Above are the matching files for $line in $file. Add it manually, the press [ENTER] to continue..."
        elif [[ $(wc -l tmp.txt | cut -d' ' -f1) -eq 0 ]]; then
            debug "l3" "No matching files found for $line in $file! Please find and add manually!"
            echo "$line /// $file" | tee -a "$manuals" 1>/dev/null
        else
            cat tmp.txt | tee -a "$pFile" 1>/dev/null
        fi
        rm tmp.txt
    done <"$file"
    debug "l1" "Done processing playlist $file!"
done

debug "l1" "Done processing all playlists!"
#EOF