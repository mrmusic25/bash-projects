#!/bin/bash
#
# compilerScript.sh - A script that will compile source code given and output to given directory
#
# Usage: cs [options] <mainFile>
# See displayHelp for more info
#
# Changes
# v2.0.1
# - Script is ready and passes shellcheck, ready for testing and release!
#
# v2.0.0
# - Begin work on overhaul for modernization and support of more languages
#
# v1.1.3
# - Changed all instances of 'ming32' to 'mingw32', watch for errors
#
# v1.1.2
# - Forgot to add error checking for the -c option
# - Easiest way to do above is to check against an array of options, found containsElement() on StackOverflow
# - Despite my beliefs, I switched displayHelp() to use spaces, since tabs can mess up formatting
#
# v1.1.1
# - Added '-static' to mingw options, otherwise it doesn't work on Windows
# - Added -c option to script, adds whatever options the user wants to the compiler
#
# v1.1.0
# - Full mingw support, though some options are not available
#
# v1.0.1
# - Started adding support for mingw32
# - Changed options to use gcc again, turns out g++ is just gcc with special options
#
# v1.0.0
# - Release version ready. NO mingw32 support (yet)
# - Note: While everything refers to gcc, the compiler used is actually g++
#
# v0.0.2
# - Decided options was necessary, added processArgs()
# - Trying out a new method for displayHelp()
#
# v0.0.1
# - Initial version
#
# v2.0.1, 11 Feb. 2020 16:35 PST

### Variables

export shortName="cs"
export longName="compilerScript"
export aChar="^"
mainFile="null" # Main file to be compiled
   outputDir="" # Where final executable will be sent, current directory by default
   outputName="a" # Executable filename
   outputFormat=".out" # File extension of executable. Including dot since Linux doesn't use dot by default
   outputArch=32 # 32 or 64. Might add support for ARM later
   outputTarget="linux" # Linux or Windows. Changes compiler, if applicable
compilerMode="debug" # Debug or release version (where applicable)
compilerOptions="" # Options to be added to compiler
continuousCompile=1 # Tells script to run indefinitely, until user indicates to stop

### Functions

if [[ -f commonFunctions.sh ]]; then
	source commonFunctions.sh
elif [[ -f /usr/share/commonFunctions.sh ]]; then
	source /usr/share/commonFunctions.sh
else
	echo "commonFunctions.sh could not be located!"
	
	# Comment/uncomment below depending on if script actually uses common functions
	echo "Script will now exit, please put file in same directory as script, or link to /usr/share!"
	exit 1
fi

function containsElement () {
	# Thanks, Patrik on StackOverflow! I improved his version by switching double space to tabs! =)
	local e
	for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
	return 1
}

function processArgs() {
	# If no arguments present, display help and exit
	if [[ $# -eq 0 ]]; then
		debug "l2" "ERROR: No options given, please fix and re-run!"
		displayHelp
		exit 1
	fi
	
	while [[ ! -z $1 ]]; do
		if [[ -f "$1" ]]; then
         debug "INFO: Assuming file is last argument and returning!"
         mainFile="$1"
         if [[ "$outputName" == "a" ]]; then
            debug "WARN: Output name not set, using beginning of mainFile!"
            outputName="$(printf "%s" "$mainFile" | cut -d'.' -f1)"
         fi
         return 0
      fi
      
      case "$1" in
		-h|--help)
		displayHelp
		exit 0
		;;
      -m|--mode)
      if ( -z $2 || [[ "$2" != "debug" && "$2" != "release" ]]); then
         debug "l2" "ERROR: Invalid option ($2) given to $1! Please fix and re-run!"
         displayHelp
         exit 1
      fi
      compilerMode="$2"
      debug "INFO: Compiler mode switched to $compilerMode"
      shift
      ;;
      -o|--output-name)
      # As long as $2 is not an option, assume it is a valid name
      if [[ "$2" == -* ]]; then
         debug "l2" "ERROR: No name given with option $1! Please fix and re-run!"
         displayHelp
         exit 1
      fi
      outputName="$2"
      shift
      ;;
      -d|--directory)
      if [[ -z $2 || ! -d "$2" ]]; then
         debug "l2" "ERROR: No directory given with option $1! Please fix and re-run!"
         displayHelp
         exit 1
      elif [[ "$2" != */ ]]; then
         outputDir="$2/" # Needs to end like this for output to work properly
      else
         outputDir="$2"
      fi
      shift
      ;;
      -a|--arch)
      debug "INFO: Switching to 64-bit architecture!"
      outputArch=64
      ;;
      -w|--windows)
      debug "INFO: Outputting a Windows executable!"
      outputTarget="windows"
      ;;
      -t|--cont*|-cc) # Secret option -cc for "continuous compile"!
      debug "l2" "WARN: Enabling continuous compile mode!"
      continuousCompile=0
      ;;
      -c|--compiler-option)
      # We can't check for options, as this will mostly be used for things like -iboostlib, etc.
      if [[ -z $2 ]]; then
         debug "l2" "ERROR: No options given with $1! Please fix and re-run!"
         displayHelp
         exit 1
      fi
      compilerOptions="$compilerOptions $2"
      shift
      ;;
      *)
		debug "Unknown option given: $1"
		announce "Unknown option given!" "Option: $1" "Please fix and re-run!"
		displayHelp
		exit 1
		;;
		esac
		shift
	done
}

function displayHelp() {
read -d '' helpVar <<"endHelp"

Usage: cs [options] <mainFile>
Note: This script works best for simple projects, in the directory where all files are located

Options:
   -h | --help                      : Print this help message and exit.
   -m | --mode <debug|release>      : Switches the compiler mode. Debug by default.
   -o | --output-name <string>      : Output name for executable; Defaults to mainFile name minus the extension.
   -d | --directory <dir>           : Outputs final executable to specified directory
   -a | --arch                      : Switches from 32-bit to 64-bit architecture for the complier. 
   -w | --windows                   : Outputs a Windows version executable instead of Linux/BSD/OSX version.
   -c | --compiler-option <option>  : Adds given option to the compiler. Be careful, this can be unforgiving!
   -t | --continuous-compile        : Script stays active until told to quit, will re-compile after user makes external changes
	
Script will automatically try to run based on file given.
Currently supported languages:
  C++, Java (JARs must be done manually atm), Bash/Shell (shellcheck)
  
For continuous compile, script assumes you are using an external IDE (that is not waiting on a return value)
   Also works with a tabbed terminal, or perhaps screen
   After compilation, script will wait for user input, then re-compile changes
   When user indicates to quit, script will offer to re-compile in release mode, if desired
endHelp
echo "$helpVar"
}

function determineLanguage() {
   # Hate doing a list of if statements, but it makes it easier to extend later
   if [[ ! -z $outputLanguage ]]; then
      debug "WARN: determineLanguage() has already been run!"
      return 0
   elif [[ "$mainFile" == *.cpp || "$mainFile" == *.hpp ]]; then # Remember to code in specific support for header files! 
      debug "INFO: Detected language is C++!"
      checkRequirements "g++" "gdb"
      export outputLanguage="c++"
      if [[ "$outputTarget" == "windows" ]]; then # Assume linux
         checkRequirements "i686-w64-ming32-g++/mingw-w64"
         outputFormat=".exe"
      else
         outputFormat=""
      fi
   elif [[ "$mainFile" == *.java ]]; then
      debug "INFO: Detected language is Java!"
      checkRequirements "javac/openjdk-8-jdk" # NOTE: If you need JDK 11 or newer, or a different one entirely, you will have to install it manually!
      export outputLanguage="java"
      outputFormat=".class" # jar building not supported at this time; I'm not very good at Java
   elif [[ "$mainFile" == *.sh || "$mainFile" == *.bash ]]; then
      debug "INFO: Detected languages is Bash!"
      checkRequirements "shellcheck"
      export outputLanguage="bash"
      outputFormat=".sh" # Not needed, but still
   elif [[ "$mainFile" == .h ]]; then
      debug "l2" "ERROR: Main file $mainFile is too ambiguous! Please fix or compile manually!"
      displayHelp
      exit 1
   else
      debug "l2" "FATAL: Main file $mainFile was not recognized! Unable to continue!"
      displayHelp
      exit 1
   fi    
}

function compileProgram() {
   # Assumes everything has been set beforehand
   local compileCommand=""
   
   # C++
   if [[ "$outputLanguage" == "c++" ]]; then
      # Windows vs normal compiler
      if [[ "$outputTarget" == "windows" ]]; then
         if [[ "$outputArch" -eq 32 ]]; then # I have made my bed, and now I will sleep in it. 
            compileCommand="i686-w64-mingw32-g++"
         else
            compileCommand="x86_64-w64-mingw32-g++"
         fi
      else
         compileCommand="g++"
         
         # Arch target
         if [[ "$outputArch" -eq 32 ]]; then
            compileCommand="$compileCommand -m32"
         else
            compileCommand="$compileCommand -m64"
         fi
      fi
      
      # Add debug mode
      if [[ "$compilerMode" == "debug" ]]; then
         compileCommand="$compileCommand -g"
         #else assume release, no extra option needed
      fi
      
      # Add any compiler options
      compileCommand="$compileCommand $compilerOptions"
      
      # Add the output
      compileCommand="$compileCommand -o $outputDir""$outputName""$outputFormat" # Done this way so dot isn't mandatory
   elif [[ "$outputLanguage" == "bash" ]]; then
      compileCommand="shellcheck" # That's it. The only command. Ignor all else. No output either.
   elif [[ "$outputLanguage" == "java" ]]; then
      compileCommand="javac"
      
      # No arch support, but debug yes
      if [[ "$compilerMode" == "debug" ]]; then
         compileCommand="$compileCommand -g"
      fi
      
      # Add output dir for classes if specified, hope user knows what they're doing
      if [[ "$outputDir" != "" ]]; then
         compileCommand="$compileCommand -d $outputDir"
      fi
      
      # Thankfully we don't have to code for Windows support since Java is platform agnostic =)
   fi
   
   # For all compilers, file goes at the end thankfully (change if more language support is added!)
   compileCommand="$compileCommand $mainFile"
   
   # Finally, run it and return the same value as the compiler
   debug "INFO: Attempting compilation with: $compileCommand"
   eval "$compileCommand"
   local rval="$?"
   
   # Report errors
   if [[ "$rval" -ne 0 ]]; then
      debug "l2" "WARN: Something happened during compilation, return code $rval!"
   fi
   
   return "$rval"
}

### Main Script

# Instead of installing all requirements at beginning of script, I am opting to install them as needed
# Saves disk space, while sacrificing time for first run. A worthy sacrifice IMO.
# This also allows extensibility by only installing compilers as needed

# Check to see if script is linked
if [[ ! -e /usr/bin/cs ]]; then
   debug "l2" "WARN: Script is not linked, linking now!"
   absPath="$(pwd)/compilerScript.sh" # NOTE: This assumes first time run is in the same directory! Should be fine 99% of the time
   dynamicLinker "$absPath"
fi

# Now run do everything else
processArgs "$@"
determineLanguage

if [[ "$continuousCompile" -eq 0 ]]; then
   while [[ "$continuousCompile" -eq 0 ]]; do
      compileProgram
      if ! getUserAnswer "Type yes to continuing compiling, no to quit"; then
         debug "INFO: Ending continuous compile mode!"
         continuousCompile=1
         if getUserAnswer "Would you like to compile one last time in release mode?"; then
            debug "INFO: Changing to release mode and compiling!"
            compilerMode="release"
            compileProgram
            fval="$?"
         fi
      fi
   done
else
   compileProgram
   fval="$?"
fi

debug "INFO: Final return value: $fval"
exit "$fval" # This step is important as I might tell VSCode to use this script
#EOF