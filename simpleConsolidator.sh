#!/usr/bin/env bash
#
# simpleConsolidator.sh - A script to move all files of given file type to a single folder
#
# Usage: sC.sh <destination_folder> <source_folder> [source_folder] ...
#
# v1.0.0, 31 Dec. 2020 17:12 PST

### Prep

export shortName="sC"
export longName="simpleConsolidator"

if ! source /usr/share/commonFunctions.sh; then
    echo "ERROR: Could not load /usr/share/commonFunctions.sh! Please install from gitlab.com/mrmusic25/linux-pref and re-run!" >&2
    exit 1
fi

### Vars

export outputFolder="$HOME/simpleConsolidator"
export allowedExt=(jpg jpeg png gif mov 3g2 3gp mkv mp4 dng)

### Functions

function printUsage() {
    printf "Usage: simpleConsolidator.sh <destination_folder> <source_folder> [source_folder] ...\n" >&2
}

# Borrowed from exifRenamer.sh
function isAllowed() {
    if [[ -z $1 ]]; then
        debug "l3" "No file given to isAllowed()! Assuming not allowed, attempting to continue..."
        return 1
    elif [[ ! -e "$1" ]]; then
        debug "l4" "$1 is not a file! Cannot determine if allowed, assuming no!"
        return 1
    elif [[ $(echo "$1" | tr -d -c '.' | wc -c) -lt 1 ]]; then
        debug "l3" "$1 does not have an extension! Assuming not allowed..."
        return 1
    fi

    local ext="$(echo "$1" | rev | cut -d'.' -f1 | rev | awk '{print tolower($0)}')"
    for extension in ${allowedExt[@]}; do
        if [[ "$ext" == "$extension" ]]; then
            return 0 # Found a match, allow file
        fi
    done
    return 1
}

function processFolder() {
    if [[ -z $1 ]]; then
        debug "l4" "No argument given to processFolder()! Please fix and re-run!"
        return 1
    elif [[ ! -d "$1" ]]; then
        debug "l4" "Argument $1 is not a folder! Please fix and re-run!"
        return 1
    fi

    # Make sure folder is accessible
    local OPWD="$(pwd)"
    if ! cd "$1"; then
        debug "l4" "Could not change into $1, does $USER have permission? Attempting to continue..."
        return 1
    else
        debug "l1" "Beginning work on folder $(pwd)!"
    fi

    # Now, process each file
    for file in *; do
        if [[ -d "$file" ]]; then
            processFolder "$file"
        else
            if isAllowed "$file"; then
                debug "l0" "Copying $file to $outputFolder"
                if ! cp "$file" "$outputFolder"; then
                    debug "l3" "Could not copy file! Does $USER have permission? Attempting to continue..."
                    # Not returning here since it could just be a single file with a permission error
                else
                    debug "l1" "Copied $file to $outputFolder successfully!"
                fi
            else
                debug "l0" "File extension of $file is not allowed, skipping!"
            fi
        fi
    done

    cd "$OPWD"

    debug "l1" "Done processing folder $1!"
    return 0
}

### Main

if [[ -z $1 || -z $2 ]]; then
    debug "l4" "Invalid number of arguments given! Please fix and re-run!"
    printUsage
    exit 1
fi

# Asign outputFolder
export outputFolder="$1"
if [[ "$outputFolder" != /* ]]; then
    debug "l2" "Given folder $outputFolder is not a full path, assuming user wants current directory $(pwd)!"
    export outputFolder="$(pwd)/$outputFolder"
fi

debug "l1" "Using $outputFolder as destination folder"
if ! mkdir --parents "$outputFolder"; then
    debug "l5" "Could not create directory $outputFolder, does $USER have permission? Exiting!"
    exit 1
else
    debug "l0" "$outputFolder successfully created"
fi

# Now, process remaining arguments as folders
shift # Get rid of trash

while [[ ! -z $1 ]]; do
    if ! processFolder "$1"; then
        debug "l3" "A problem occurred while processing $1, see log for details! Attempting to continue..."
    else
        debug "l1" "Successfully processed argument $1!"
    fi
    shift
done

debug "l1" "Done with script!"

#EOF