#!/usr/bin/env bash
#
# Usage: ./syncMusic.sh <musicDirectory> [baseDirectory]
#
# Run script, then follow prompts
#
# v0.1.0, 11 Aug. 2021 21:23 PDT

### Variables

base="$HOME"                            # Main folder where output files will be kept
musicDirectory=""                       # Location of the music folder
date="$(date +%b-%d-%y_%H-%M)"         # Date to be appended to each file
newFiles="$base/newFiles-$date.txt"     # Location of new files added
delFiles="$base/deletedFiles-$date.txt" # Files that have been deleted since last sync
diffFile="$base/diff-$date.txt"         # Location of the main diff file
oldIndex="$base/oldIndex-$date.txt"     # Index before sync
newIndex="$base/newIndex-$date.txt"     # Index after sync
dryRun=0                                # Switch to 1 to only show files that WOULD be touched

### Functions

function usage() {
    printf "\nUsage: ./syncMusic.sh <musicDirectory> [baseDirectory]\n   Files made from diff will be stored in baseDirectory, \$HOME by default.\n\n"
}

### Main

if [[ -z $1 ]]; then
    usage
    exit 1
elif [[ ! -d "$1" ]]; then
    echo "ERROR: $1 is not a directory!"
    usage
    exit 1
else
    musicDirectory="$1"
fi

if [[ -n $2 ]]; then
    if [[ ! -d $2 ]]; then
        echo "ERROR: Given base directory $2 is not a folder!"
        usage
        exit 1
    else
        base="$2"
        newFiles="$base/newFiles-$date.txt"
        delFiles="$base/deletedFiles-$date.txt"
        diffFile="$base/diff-$date.txt"
        oldIndex="$base/oldIndex-$date.txt"
        newIndex="$base/newIndex-$date.txt"
    fi
fi

autoFolder="$(echo "$musicDirectory" | rev | cut -d'/' -f1 --complement | rev)/Automatically Add to iTunes"

printf "* * * Now running index on music folder %s, please wait!\n" "$musicDirectory"

find "$musicDirectory" -type f > "$oldIndex"

read -p "* * * Index complete! Please close iTunes, then syncronize folders externally and press [ENTER] when ready to continue..." ready

printf "* * * Now taking index of syncronized folder, please wait!\n"

find "$musicDirectory" -type f > "$newIndex"

printf "* * * Done taking new index, creating diff files, please wait!\n"

diff "$oldIndex" "$newIndex" > "$diffFile"
grep ^\< "$diffFile" > "$delFiles"
grep ^\> "$diffFile" > "$newFiles"

printf "* * * Done creating diffs, new files can be found at %s and deleted files can be found at %s. Now copying new files to iTunes automatic folder, press CTRL+C within 5 seconds to cancel!" "$newFiles" "$delFiles"
sleep 5s

printf "* * * Copying new files to auto folder at %s!\n" "$autoFolder"

while read -r line; do
    if [[ -n "$(echo "$line" | grep -E "(mp3|m4a|m4p)$")" ]]; then
        f="$(echo "$line" | cut -d' ' -f1 --complement)"
        printf "Moving file %s to folder %s" "$f" "$autoFolder\n"
        if [[ "$dryRun" -eq 0 ]]; then
            mv -v "$f" "$autoFolder"
        fi
    fi
done < "$newFiles"

printf "* * * Done with script!\n"

exit 0

#EOF