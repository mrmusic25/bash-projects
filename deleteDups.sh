#!/usr/bin/env bash
#
# deleteDups.sh - A scritp that will recureisvely go through a folder and delete any duplicate hashes found
#      ~ Optionally, move duplicates to a trash folder for verification
#
# Usage: deleteDups.sh <folder> [trash_folder]
#    ~ If trash_folder is given, it will use it. Else, deletion will be assumed
#
# v1.0.2 by Kyle Krattiger, 11 Feb. 2021 01:45 PST

if ! source /usr/share/commonFunctions.sh; then
    echo "ERROR: Script relies on commonFunctions.sh! Please install and re-run!"
    exit 1
fi

### Vars

declare -a hashList           # Where sha256 hashes will be stored
export trashFolder="ddTrash"  # Directory for trash files
export dupAction="delete"     # "delete" to delete duplicates, "trash" to send items to trashFolder

### Functions

function removeFile() {
    if [[ -z $1 ]]; then
        debug "l3" "No arugment given to removeFile()! Attempting to continue..."
        return 1
    elif [[ ! -f $1 ]]; then
        debug "l3" "$1 is not a regular file! Returning..."
        return 1
    fi

    if [[ "$dupAction" == "delete" ]]; then
        debug "l2" "Permanently deleting file $1!"
        if ! rm "$1"; then
            debug "l4" "Could not delete $1, does $(whoami) have permission? Attempting to continue..."
            return 1
        else
            return 0
        fi
    elif [[ "$dupAction" == "trash" ]]; then
        debug "l2" "Moving file $1 to trash!"
        if [[ ! -d "$trashFolder" ]]; then
            if ! mkdir -p "$trashFolder"; then
                debug "l4" "Unable to create trash directory $trashFolder! Dose $(whomi) have permission? Please fix and re-run!"
                exit 1
            fi
        fi
        if ! mv "$1" "$trashFolder"/; then
            debug "l3" "Unable to move file $1 to trash folder $trashFolder! Does $(whoami) have permission? Attempting to continue..."
            return 1
        else
            return 0
        fi
    else
        debug "l4" "Unknown dupAction: $dupAction! Exiting..."
        exit 1
    fi
    return 1 # Shouldn't ever make it this far, but report an error if so
}

function exists() {
    if [[ -z $1 ]]; then
        debug "l3" "No arugment given to exists()! Attempting to continue..."
        return 1
    elif [[ ! -f $1 ]]; then
        debug "l3" "$1 is not a regular file! Returning..."
        return 1
    fi

    local fhash="$(sha256sum "$1" | cut -d' ' -f1)"
    for thash in "${hashList[@]}"; do
        if [[ "$fhash" == "$thash" ]]; then
            return 0
        fi
    done

    # Does not exists, add to list and return
    hashList+=("$fhash")
    return 1
}

function processFolder() {
    if [[ -z $1 ]]; then
        debug "l3" "No arugment given to processFolder()! Attempting to continue..."
        return 1
    elif [[ ! -d "$1" ]]; then
        debug "l4" "$1 is not a folder, cannot be processed!"
        return 1
    else
        debug "l1" "Working on folder $(pwd)/$1..."
    fi
    
    local OPWD="$(pwd)"
    if ! cd "$1"; then
        debug "l3" "Unable to change into $1! Does $(whoami) have permission? Returning..."
        return 1
    fi

    local dfile
    for dfile in *; do
        if [[ -d "$dfile" ]]; then
            processFolder "$dfile"
        elif [[ -f "$dfile" ]]; then
            if exists "$dfile"; then
                debug "l2" "$dfile already exists in folder, attempting to remove!"
                if ! removeFile "$dfile"; then
                    debug "l2" "An error occurred while trying to remove $dfile, attempting to continue!"
                else
                    debug "l0" "File $dfile successfully processed"
                fi
            else
                debug "l0" "File $dfile is not a duplicate! Moving on..."
            fi
        else
            debug "l2" "$dfile is not a regular file or folder! Ignoring, process manually!"
        fi
    done
    if ! cd "$OPWD"; then
        debug "l3" "Unable to change back to intial directory $OPWD! Returning an error..."
        return 1
    fi
    
    debug "l1" "Finished working on folder $(pwd)/$1!"
    return 0
}

### Main

if [[ -z $1 ]]; then
    debug "l4" "No arguments given to script! Exiting..."
    printf "Usage: deleteDups.sh <folder> [trash_folder]\n" >&2
    exit 1
elif [[ ! -z $2 ]]; then
    debug "l2" "Using $2 as the trash folder and enabling trash mode for file removal!"
    if [[ "$2" != /* ]]; then
        debug "l2" "Trash folder is not a full path, assuming current directory..."
        export trashFolder="$(pwd)/$2"
    else
        export trashFolder="$2"
    fi

    if ! mkdir --parents "$trashFolder"; then
        debug "l4" "Could not create trash folder $trashFolder! Please fix and re-run!"
        exit 1
    fi
    export dupAction="trash"
fi

if ! processFolder "$1"; then
    debug "l4" "Given argument $1 could not be processed, see log for more info! Exiting..."
    printf "Usage: deleteDups.sh <folder> [trash_folder]\n" >&2
    exit 1
else
    debug "l1" "Successfully processed folder $1"
fi

debug "l1" "Done processing initial folder $1, exiting script!"

#EOF