#!/usr/bin/env bash
#
# findMusic.sh - Program to scan and find missing songs from a text-based playlist file (.txt, .m3u, .m3u8, etc.)
#
# Changelog:
# v0.2.3
# - Script will add newlines to playlists if needed
# - Beat my record for most version increments in the span of 20 minutes lol
#
# v0.2.2
# - Added some more use cases to locate() based just on song number and corresponding album
#
# v0.2.1
# - Fixed some typos
# - Added a pause when processing a folder so user can add missing songs
# - More verbosity
#
# v0.2.0
# - Added a toWindows() function to convert path back to Windows format
# - Implemented said function into process()
#
# v0.1.1
# - Fixed a couple boolean errors
# - Added folder processing
#
# v0.1.0
# - Initial release
# - Ready for testing!
#
# Usage: findMusic.sh [-v|--verbose] <music_folder> <playlist_file> [playlist_file]
#   New playlists will be output to the same location as the original, with '-updated' appended to the filename
#
# v0.2.3, 08 Mar. 2021, 00:57 PST

### Setup

shortName="fm"
longName="findMusic"
aChar="~"

if ! source /usr/share/commonFunctions.sh; then
    echo "Could not source commonFunctions.sh! Please download from https://gitlab.com/mrmusic25/linux-pref and re-try!"
    exit 1
fi

### Vars

musicFolder="/"    # Location of the music library, typically C:\Users\<username>\Music on Windows
folderScanned=1
indexFile="$(pwd)/fmIndex.txt"

### Functions

function usage() {
    printf "\nUsage: findMusic.sh [-v|--verbose] <music_folder> <playlist_file> [playlist_file]\n" 1>&2
    printf "   New playlists will be output to the same location as the original, with '-updated' appended to the filename\n\n" 1>&2
}

function process() {
    # Sanity check
    if [[ -z "$1" ]]; then
        debug "l3" "No file given to process()! Please fix and re-run!"
        return 1
    fi

    # Scan folder if it hasn't been scanned
    if [[ "$folderScanned" -ne 0 ]]; then
        debug "l2" "Creating an index of folder $musicFolder"
        find "$musicFolder" -type f > "$indexFile"
        debug "l1" "Finished making index"
        folderScanned=0
    fi

    # Now process each line in file
    local basefile="$(echo "$1" | rev | cut -d'/' -f1 | rev)"
    local basename="$(echo "$basefile" | rev | cut -d'.' -f1 --complement | rev)"
    local ext="$(echo "$1" | rev | cut -d'.' -f1 | rev)"
    if [[ "$1" == */* ]]; then
        local newFile="$(echo "$1" | rev | cut -d'/' -f1 --complement | rev)/$basename-updated.$ext"
    else
        local newFile="$basename-updated.$ext"
    fi

    # Remove existing new file, if it exists
    if [[ -f "$newFile" ]]; then
        debug "l2" "Removing existing file $newFile for new variation..."
        if ! rm "$newFile"; then
            debug "l3" "Could not remove $newFile! Does $USER have permission? Stopping processing of $1..."
            return 1
        fi
    fi

    # Convert to useable Unix format and add newline to EOF, is missing
    dos2unix "$1"
    if [[ -n "$(tail -c 1 "$1")" ]]; then
        printf "\n" | tee -a "$1" 1>/dev/null
    fi

    # Using this, process each line in the given file
    local tmp=""
    while read -r line; do
        if [[ "$line" == \#* || -f "$line" ]]; then
            echo "$line" | tee -a "$newFile" 1>/dev/null
        else
            tmp="$(locate "$line")"
            if [[ "$tmp" == \#* ]]; then
                debug "l2" "Could not find a file similar to $line! Please find manually!"
            else
                tmp="$(toWindows "$tmp")"
                debug "l0" "Found $tmp from $line"
            fi
            echo "$tmp" | tee -a "$newFile" 1>/dev/null
        fi
    done < "$1"
    return 0
}

function locate() {
    # Sanity check
    if [[ -z $1 ]]; then
        debug "l3" "No file given to locate()!"
        echo "# No file given to locate()!"
        return 1
    fi

    # Now, find the given file
    # Most common: convert from Win to Unix path
    local lfile="$(win2UnixPath "$1" "/mnt")"
    if [[ -f "$lfile" ]]; then
        echo "$lfile"
        return 0
    fi

    # Else, search through index for most likely candidate
    local tmpIndex="$(pwd)/tmpIndex.txt"
    local count=0
    local lFlag=0
    local lineNum=0
    local song="$(echo "$lfile" | rev | cut -d'/' -f1 | rev)"
    local justNum="$(echo "$song" | cut -d' ' -f1)"
    local album="$(echo "$lfile" | rev | cut -d'/' -f2 | rev)"
    local artist="$(echo "$lfile" | rev | cut -d'/' -f3 | rev)"
    local songNoExt="$(echo "$song" | rev | cut -d'.' -f1 --complement | rev)"

    if [[ $(echo "$song" | head -c 1) -eq $(echo "$song" | head -c 1) ]]; then
        local songNoNum="$(echo "$song" | cut -d' ' -f 2)"
    else
        local songNoNum="$song"
    fi
    local songNoNumExt="$(echo "$songNoNum" | rev | cut -d'.' -f1 --complement | rev)"

    # Makes all grep cases case-insensitive
    alias grep='grep -i'
    
    # Loop through trying different possibilities
    while [[ "$lFlag" -eq 0 ]]; do
        if [[ -f "$tmpIndex" ]]; then
            rm "$tmpIndex"
        fi

        case "$count" in
            0) # Just the song name with number
            grep "$song" "$indexFile" > "$tmpIndex"
            ;;
            1) # Check for artist name first, then song
            grep "$artist" "$indexFile" | grep "$song" > "$tmpIndex"
            ;;
            2) # Artist, Album, then song
            grep "$artist" "$indexFile" | grep "$album" | grep "$song" > "$tmpIndex"
            ;;
            3) # Artist, album, song (no num)
            grep "$artist" "$indexFile" | grep "$album" | grep "$songNoNum" > "$tmpIndex"
            ;;
            4) # Album, song (possible for "various artist" and "compilations" folders)
            grep "$album" "$indexFile" | grep "$song" > "$tmpIndex"
            ;;
            5) # Album, song (no num)
            grep "$album" "$indexFile" | grep "$songNoNum" > "$tmpIndex"
            ;;
            6) # Song (no ext, in case the extension has changed)
            grep "$songNoExt" "$indexFile" > "$tmpIndex"
            ;;
            7) # Artist, song (no ext)
            grep "$artist" "$indexFile" | grep "$songNoExt" > "$tmpIndex"
            ;;
            8) # Album, song (no ext)
            grep "$album" "$indexFile" | grep "$songNoExt" > "$tmpIndex"
            ;;
            9) # Artist, album, song (no ext)
            grep "$artist" "$indexFile" | grep "$album" | grep "$songNoExt" > "$tmpIndex"
            ;;
            10) # Song (no num/ext)
            grep "$songNoNumExt" "$indexFile" > "$tmpIndex"
            ;;
            11) # Album, song (no num/ext)
            grep "$album" "$indexFile" | grep "$songNoNumExt" > "$tmpIndex"
            ;;
            12) # Artist, album, song (no num/ext)
            grep "$artist" "$indexFile" | grep "$album" | grep "$songNoNumExt" > "$tmpIndex"
            ;;
            13) # Artist, album and song number (in case filename has change or isn't English)
            grep "$artist" "$indexFile" | grep "$album" | grep "$justNum" > "$tmpIndex"
            ;;
            14) # Album, song number
            grep "$album" "$indexFile" | grep "$justNum" > "$tmpIndex"
            ;;
            15) # If script makes it this far, user needs to manually find it
            echo "# Could not find file based off $1! Please find manually!" > "$tmpIndex"
            lFlag=1
            ;;
        esac

        lineNum="$(wc -l "$tmpIndex" | cut -d' ' -f 1)"
        if [[ "$lineNum" -eq 1 ]]; then
            lFlag=1 # File has been found
        else
            ((count++))
        fi
    done
    
    # Done, output result (should only be a single item)
    head -n1 "$tmpIndex"
    unalias grep
    rm "$tmpIndex"

    return 0
}

function toWindows() {
    if [[ -z $1 ]]; then
        debug "l3" "Nothing given to toWindows()!"
        return 1
    fi

    #local prefix="/mnt/f"
    local winPrefix="F:"
    local filename="$1"

    # First, get rid of the prefix and convert '/' to '\'
    filename="$(echo "$filename" | sed -e 's/\/mnt\/f//g' -e 's/\//\\/g')"

    # Add the Windows and print it
    filename="$winPrefix$filename"
    echo "$filename"
    return 0
}

### Main

# Sanity check
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    usage
    exit 0
fi

if [[ "$1" == "-v" || "$1" == "--verbose" ]]; then
    debugLevel=1
    shift
elif [[ "$1" == "-vv" || "$1" == "--vverbose" ]]; then
    debugLevel=0
    shift
fi

if [[ ! -d "$1" ]]; then
    debug "l4" "First argument $1 is not a directory! Please fix and re-run!"
    usage
    exit 1
else
    musicFolder="$1"
    debug "l1" "Now using $musicFolder as the music folder"
    shift
fi

# Now, loop through each given playlist
if [[ -z "$1" ]]; then
    debug "l3" "No files given to process! Exiting..."
    usage
    exit 1
else
    while [[ -n "$1" ]]; do
        if [[ -d "$1" ]]; then
            debug "l2" "Argument $1 is a directory, processing each file within..."
            OPWD="$(pwd)"
            if ! cd "$1"; then
                debug "l3" "Could not change into $1, does $USER have permission? Exiting!"
                usage
                exit 1
            fi
            if ! rm *-updated*; then
                debug "l2" "Could not remove previously updated playlists! Attempting to continue..."
            fi
            for file in *; do
                if ! process "$file"; then
                    debug "l3" "A problem occurred while processing $file! Attempting to continue..."
                else
                    debug "l1" "Successfully processed $(pwd)/$file!"
                fi
                pause "Press [Enter] when ready to continue..."
            done
            cd "$OPWD"
        elif [[ ! -f "$1" ]]; then
            debug "l2" "$1 is not a valid filename! Skipping..."
        elif ! process "$1"; then
            debug "l3" "A problem occurred while processing $1! Attempting to continue..."
        else
            debug "l1" "Successfully processesed playlist $1"
        fi
        shift
    done
fi

debug "l1" "Done with script!"
        
#EOF