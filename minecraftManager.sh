#!/usr/bin/env bash
#
# minecraftManager.sh - A script to control Minecraft server(s) on a system
#
# v0.1.0, 27 Dec. 2020 13:40 PST

### Prep

export shortName="MM"
export longName="minecraftManager"

if ! source /usr/share/commonFunctions.sh; then
    echo "ERROR: Could not load /usr/share/commonFunctions.sh! Please install from gitlab.com/mrmusic25/linux-pref and re-run!" >&2
    exit 1
fi

### Vars

configLocation="$HOME/.minecraftManager.conf"  # Location of the main config file for the script

### Functions



### Main



#EOF