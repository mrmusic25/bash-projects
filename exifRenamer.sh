#!/bin/bash
#
# exifRenamer.sh - A script to rename pictures and videos based on their EXIF data
#
# v1.0.2, 01 Nov. 2020 14:01 PST

source /usr/share/commonFunctions.sh
export debugLevel=1

### Global vars

outFormat="%Y-%m-%d_%H-%M-%S%%-c.%%e"
allowedExt=(jpg jpeg png gif mov 3g2 3gp mkv mp4 dng) # List of allowed extensions

### Functions

function isAllowed() {
    if [[ -z $1 ]]; then
        debug "l3" "No file given to isAllowed()! Assuming not allowed, attempting to continue..."
        return 1
    elif [[ ! -e "$1" ]]; then
        debug "l4" "$1 is not a file! Cannot determine if allowed, assuming no!"
        return 1
    elif [[ $(echo "$1" | tr -d -c '.' | wc -c) -lt 1 ]]; then
        debug "l3" "$1 does not have an extension! Assuming not allowed..."
        return 1
    fi

    local ext="$(echo "$1" | rev | cut -d'.' -f1 | rev | awk '{print tolower($0)}')"
    for extension in ${allowedExt[@]}; do
        if [[ "$ext" == "$extension" ]]; then
            return 0 # Found a match, allow file
        fi
    done
    return 1
}

function processFolder() {
    if [[ -z $1 ]]; then
        debug "l3" "No folder given to processFolder()! Please fix and re-try!"
        return 1
    elif [[ ! -d "$1" ]]; then
        debug "l3" "$1 is not a folder! Returning..."
        return 1
    fi

    local OPWD="$(pwd)"
    if ! cd "$1"; then 
        debug "l3" "Could not change into $1, does user have permission? Returning..."
        return 1
    fi

    local file
    for file in *; do
        if [[ -d "$file" ]]; then
            if ! processFolder "$(pwd)"/"$file"; then
                debug "l2" "An error occurred while processing $file, see log for more info!"
            else
                debug "l1" "Done with recursive folder $file!"
            fi
        elif isAllowed "$file"; then
            # Determine EXIF tag to be used
            local tag
            if [[ ! -z $(exiftool -DateTimeOriginal "$file" 2>/dev/null) ]]; then
                tag="DateTimeOriginal"
            elif [[ ! -z $(exiftool -ContentCreateDate "$file" 2>/dev/null) ]]; then
                tag="ContentCreateDate"
            elif [[ ! -z $(exiftool -DateAcquired "$file" 2>/dev/null) ]]; then
                tag="DateAcquired"
            else
                tag="FileModifyDate" # Last-ditch effort so everything has unique-ish name
            fi

            # Match the creation and modify times with the EXIF data using the same tag
            if ! exiftool -FileAccessDate\<"$tag" -FileModifyDate\<"$tag" -FileInodeChangeDate\<"$tag" "$file" 1>/dev/null 2>/dev/null; then
                debug "l2" "Could not change the dates for file $file!"
            fi # No need to report success on this one, only failure

            # Change file based on tags
            if ! exiftool -FileName\<"$tag" -d "$outFormat" "$file" 1>/dev/null 2>/dev/null; then
                debug "l2" "Something went wrong writing the file $file! Attempting to continue..."
            else
                debug "l1" "Successfully processed file $file!"
            fi
        else
            debug "l1" "Extension for $file is not allowed, skipping!"
        fi
    done
    cd "$OPWD"
    return 0
}

### Main

if [[ -z $1 ]]; then
    debug "l4" "No folder given to exifRenamer.sh! Please fix and re-run!"
    printf "Usage: exifRenamer.sh <folder>\nRecursively enters folder and changes filenames to the same format, based on EXIF data.\n"
    exit 1
fi

if ! processFolder "$1"; then
    debug "l3" "A problem occurred while processing given folder $1! Check log and try again!"
    exit 1
else
    debug "l1" "Successfully processed folder $1!"
fi

#EOF