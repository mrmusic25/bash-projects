#!/usr/bin/env bash
#
# Imports a CSV file and converts it to JSON
#
# NOTE: This assumes the first line of the file is the tag data!
#
# v1.0.7, 31 Jan. 2021 17:45 PST

if [[ -z $1 ]]; then
	echo "Please provide a file!"
	exit 1
elif [[ ! -f "$1" ]]; then
	echo "$1 is not a file!"
	exit 1
else
	inFile="$1"
fi

outFile="$(echo "$inFile" | rev | cut -d'.' -f1 --complement | rev).json"

declare -a tags

# Get the number of tags and split them into tags[]
tagLine="$(cat "$inFile" | sed '1q;d')"
numTags="$(echo "$tagLine" | sed -e 's/[^,]//g' | wc -c)" # Gets the number of commas in the line
i=1
while [[ i -le $numTags ]]; do
	tags+=("$(echo "$tagLine" | cut -d',' -f $i)")
	((i++))
done

# Now, process each line
printf "{\n  \"data\": [\n" > "$outFile"
firstItem=0
while read -r line; do
	if [[ -z $skipLine ]]; then # This allows us to skip the first line, where the tag data is kept (already imported)
		skipLine=0
	else
		# Only print comma if another object is pring added
		if [[ $firstItem -ne 0 ]]; then
			printf ",\n" | tee -a "$outFile" 1>/dev/null
		else
			firstItem=1
		fi
			
		printf "    {\n" | tee -a "$outFile" 1>/dev/null
		i=1
		firstTag=0
		for tag in "${tags[@]}"; do
			# Selectively print comma at the end
			if [[ $firstTag -ne 0 ]]; then
				printf ",\n" | tee -a "$outFile" 1>/dev/null
			else
				firstTag=1
			fi
			
			# Get whole tag
			key="$(echo "$line" | cut -d',' -f $i)"
			if [[ "$key" == \"* ]]; then # If the item has quotes, keep cutting until endquote is found
				until [[ "$key" == *\" ]]; do
					((i++))
					key="$key,$(echo "$line" | cut -d',' -f $i)"
				done
			fi
			
			# Make sure quotes are around strings
			if [[ "$key" != \"* ]]; then
				key="$(printf "\"%s\"" "$key")"
			fi
			
			# Remove newline/carriage return if detected
			if [[ "$tag" == *"\n\r" || "$tag" == *"\r\n" ]]; then 
				ltag="${tag::-2}" # Remove last two chars from string
			elif [[ "$tag" == *"\n" || "$tag" == *"\r" ]]; then
				ltag="${tag::-1}"
			else
				ltag="$tag"
			fi
			
			# Print full tag to the output file
			printf "      \"%s\": %s" "$ltag" "$key" | tee -a "$outFile" 1>/dev/null
			((i++))
		done
		printf "\n    }" | tee -a "$outFile" 1>/dev/null
	fi
done < "$inFile"
printf "\n  ]\n}\n" | tee -a "$outFile" 1>/dev/null

#EOF
