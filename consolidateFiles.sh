#!/bin/bash
#
# consolidateFiles.sh - A script to move all the files of many folders to one central location for processing
#                       Script will also ID and process duplicates according to user input
#
# Changes:
# 
# v1.1.1
# - Fixed a major problem with copying/moving files
# - Changes to debug statements to more easily identify them
# - Updated processFolders() to actually work
# - Fixed some typos
# - Vow to never again use [[ $var ]] over [[ $var -eq 0 ]], as this caused 75% of my problems (no joke)
#
# v1.1.0
# - Implemented keep-structures
# - Changed the way processFolders() works in order for this to be successful
# - Also had to change processFile()
#
# v1.0.1
# - Turns out the shift in processArgs() was in the wrong place after all
#
# v1.0.0
# - Initial release
# 
# v1.1.1, 10 Oct. 2019 20:55 PST

export shortName="cF"
export longName="consolidateFiles"
source /usr/share/commonFunctions.sh

### Vars

declare -a hashList # Array where existing SHA values will be kept
declare -a extList # List of extensions that will be moved/deleted
declare -a folderList # List of folder(s) to be processed
#export shortName="cF"
#export longName="consolidateFiles" # I'm not very consistent
export aChar="-"
export VERSION_CONTROL=numbered # This is for mv later, courtesy of https://serverfault.com/questions/267255/mv-rename-if-exists
outputFolder="output" # Where files and/or folder will be output
trashFolder="$HOME/trash" # Default folder to use when trash mode enabled

# Below are booleans for the script options, aptly named
# Meant to function as 'if var then; ...' so 0 for true, 1 for false. Silly bash!
interactiveMode=1
copyFolders=1 
moveFiles=1 # This is used with -m, turn it off to move files instead of copy them
deleteEmptyFolders=0
deleteMode=0 # 0=keep, 1=trash, 2=delete. Most people will likely use 0, so easier to program that in
keepStructure=1

### Functions

function displayHelp() {
read -d '' helpVar <<"endHelp"

consolidateFiles.sh - A script to consolidate all files from many folders into one

Usage: ./cF.sh [options] <output_folder> <input_folder> [input_folder] ...
                                         NOTE: You can supply as many input folders as needed
	OR ./cF.sh [options] --interactive
	
Options:
  -h | --help                             : Shows this help message and exits
  -i | --interactive                      : Starts the script in interactive mode
  -k | --keep-structure                   : Keeps file structure for similar backup folders (default=off). See example below
  -c | --copy-folders                     : Copies folders inside input_folder instead of entering folder and copying files
  -e | --extensions <extension>           : Only copies/moves specified extensions given (can be used multiple times)
                    <ext1,ext2,ext3,...>  : Can also be given a comma-delimited list
  -m | --mode <copy|move>                 : Specifies whether files will be copied (default) or moved from their old location
  -n | --no-delete-empty-folder           : Disables deleting of empty parent folders after processing (on by default)
  -d | --delete-mode <keep|trash|delete>  : Chooses how to handle duplicate files ("keep" on by default, explanation below)
  -v | --verbose                          : Enables verbose debug messaging (MUST be first argument!)

Delete Mode:
  keep   - Simply leaves the files in place, user can verify after processing and delete at their leisure
  trash  - Moves files to a given trash folder where user can verify and delete files
            NOTE: Duplicates will likely pile up, but script will handle renaming
  delete - Permanently deletes files that are duplicates (NOT recommended! Only if you have backups!)
  
-k|--keep-structure
  In the case of multiple similar backup folders, the basic file structure will remain while new files are
  all consolidated in the new folder. NOTE: in practice this means that the undelying folder of each parent folder (arguments) will be copied over.
  In the following example, F1 and F2 are combined into F3. Since Download and Downloads are different, the folders will be copied
  Despite this, only files with new SHA hashes will be copied. Hence, any empty folders will still be deleted at the end.
  {F1}/                             {F2}/                 {F3}/
   | Downloads                       | Download            | Download
   | Pictures                        | Pictures            | Downloads
                                                           | Pictures
    
endHelp
echo "$helpVar"
}

function processArgs() {
	# Make sure args given
	if [[ "$#" -lt 1 ]]; then
		debug "l2" "FATAL: Not enough arguments given to script! Please fix and re-run!"
		displayHelp
		exit 1
	fi
	
	local loopFlag=0
	while [[ "$loopFlag" -eq 0 ]];
	do
		case "$1" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-i|--interactive)
			# I plan on making/releasing interactive mode AFTER working on the rest of the script. Stay tuned!
			debug "l2" "FATAL: Interactive script is not ready yet! Please check gitlab repo for updates!"
			exit 0
			debug "INFO: Enabling interactive mode!"
			interactiveMode=0
			;;
			-c|--copy-folders)
			debug "INFO: Enabling copy-folders mode!"
			copyFolders=0
			;;
			-e|--extentions)
			if [[ -z $2 || "$2" == -* || "$2" == */* ]]; then # Not empty, not an option, not a folder
				debug "l2" "ERROR: No extension given with $1! Please fix and re-run!"
				displayHelp
				exit 1
			elif ! addExtensions "$2"; then # Errors will be spit out before it checks second condition, but oh well
				debug "l2" "FATAL: Extension(s) from $2 could not be added! Please fix and re-try!"
				displayHelp
				exit 1
			fi
			shift
			;;
			-m|--mode)
			if [[ -z $2 || "$2" == -* || "$2" == */* ]]; then
				debug "FATAL: No mode given with $1! Please fix and re-run!"
				displayHelp
				exit 1
			fi
			
			if [[ "$2" == "move" ]]; then
				debug "INFO: Enabling folder-moving"
				moveFiles=0
			elif [[ "$2" == "copy" ]]; then
				debug "INFO: Disabling folder-moving"
				moveFiles=1
			else
				debug "ERROR: Unknown option $2 fiven with $1! Please fix and re-run!"
				displayHelp
				exit 1
			fi
			shift
			;;
         -k|--keep*)
         debug "INFO: keep-structures enabled, will recreate folder structures"
         keepStructure=0
         ;;
			-n|--no-delete-empty-folder)
			debug "INFO: Script will not delete empty folders!"
			deleteEmptyFolders=1
			;;
			-d|--delete-mode)
			if [[ -z $2 || "$2" == -* || "$2" == */* ]]; then
				debug "FATAL: No mode given with $1! Please fix and re-run!"
				displayHelp
				exit 1
			fi 
			
			case "$2" in
				keep)
				deleteMode=0
				;;
				trash|garbage|recycle) # Aliases, why not
				deleteMode=1
				if ! getUserAnswer "Do you want to keep the default trash folder at: $trashFolder?"; then
					trashFolder="$(dialog --title "Select a trash folder" --stdout --title "Where all the copies of files will go" --fselect "$(pwd)" 14 48)"
				fi
				
				if [[ ! -d "$trashFolder" ]]; then
					debug "l2" "WARN: Trash folder $trashFolder does not exist, attempting to create..."
					if ! mkdir "$trashFolder"; then
						debug "l2" "FATAL: Could not create trash folder at $trashFolder! Please check permissions and re-run"
						displayHelp
						exit 1
					fi
				fi
				;;
				delete|remove)
				deleteMode=2
				;;
				*)
				debug "l2" "ERROR: Unknown option $2 given with $1! Please fix and re-run!"
				displayHelp
				exit 1
				;;
			esac
			debug "INFO: Delete mode changed to $2"
			shift
			;;
		esac
		shift
		if [[ -d "$1" || "$1" != -* ]]; then # Taking a big risk allowing this, but I can't think of a better solution (for now)
			#echo "Arg 1 is $1"
         loopFlag=1
			
			# First arg should be the output folder, just assume so since it will be created later if missing
			if [[ "$1" != /* ]]; then
				debug "l2" "WARN: Given output folder is not a full path, assuming present directory..."
				outputFolder="$(pwd)"/"$1" # Forgot this was necessary as script changes directories often
			else
				outputFolder="$1"
			fi
			debug "l5" "INFO: outputFolder is set to: $outputFolder"
			
			shift 
			if [[ "$#" -eq 0 ]]; then
				debug "l2" "FATAL: No folders to be processed were given! Please fix and re-run!"
				displayHelp
				exit 1
			fi
			
			while [[ "$#" -ne 0 ]]; 
			do
				if [[ ! -d "$1" ]]; then
					debug "FATAL: Argument $1 is not a folder! Please fix script and re-run!" # If you get this, add folders to END of arguments
					displayHelp
					exit 1
				else
					folderList+=("$1")
					shift
				fi
			done
			#echo "Output folder is $outputFolder and folders are ${folderList[@]}"
			continue
		elif [[ "$#" -lt 1 && $interactiveMode ]]; then # Hopefully `less than` was the right option to use here, quit if interactive mode only
			loopFlag=1 # Probably unnecessary, but I like to be consistent
			continue
		elif [[ "$#" -eq 0 ]]; then
			debug "l2" "FATAL: Congrats on breaking the script. Better start debugging!"
			#echo "Arguments at this point: $@" # Debug statement
			displayHelp
			exit 1
		fi
		#shift # the shift MUST be done here or else wrong folder gets pinned as outputFolder
	done
}

# Makes it easier than trying to contain it all in processArgs(). May have additional uses as the script evolves, as well.
function addExtensions() {
	if [[ -z $1 ]]; then
		debug "ERROR: No argument given to addExtension()!"
		return 1 # Thought about making this an exit statement, but then I thought better of myself
	fi
	
	local eNum="$(printf "%s" "$1" | awk -F"," '{print NF-1}')" # https://www.unix.com/shell-programming-and-scripting/142824-counting-characters-sed.html
	if [[ $eNum ]]; then # arg is not a list, add it and return successful
		debug "l5" "INFO: Adding extension $1 to extList!"
		extList+=("$(printf "%s" "$1" | awk '{print tolower($0)}' | sed 's/.//g')")
		return 0 # TODO: Check to make sure it is actually a viable extension
	fi
	
	# else, it is a list. Process it as such.
	((eNum++)) # The reason for this is so that the final field is processed from gnu/cut
	local v=""
	while [[ "$eNum" -gt 1 ]]; do
		v="$(printf "%s" "$1" | cut -d',' -f "$eNum" | awk '{print tolower($0)}' | sed 's/.//g')"
		debug "l5" "INFO: Adding #$eNum extension $v to extList!"
		extList+=("$v")
		((eNum--))
	done
	return 0
}

# Input: $1 - filename (either full or relative path)
# Returns 1 to ignore file, returns 0 to process file
# Meant to be used as `if [[ processExetnsion "$file" ]]; then process()`
function processExtension() {
	local fileExt=""
	
	# First, get the extension and store it
	if [[ -z $1 ]]; then
		debug "l2" "ERROR: No argument given to processExtension()!"
		return 1
	elif [[ "$1" != *.* ]]; then
		if [[ -f "$1" ]]; then
			debug "l5" "WARN: File $1 has no extension!"
			# TODO: Add an option to specify what to do with extensionless files. For now, allow.
			return 0
		else
			# Assume JUST the extension was given, and continue
			fileExt="$1"
		fi
	else
		fileExt="$(printf "%s" "$1" | rev | cut -d'.' -f1 | rev)"
	fi
	fileExt="$(printf "%s" "$1" | awk '{print tolower($0)}')" # Convert to lowercase for more accurate processing
	
	# Time to check if extension should be processed
	if [[ ${#extList[@]} -eq 0 ]]; then # Always process if extList is empty
		return 0
	else
		for ext in "${extList[@]}";
		do
			#echo "Checking $fileExt against $ext"
			if [[ "$fileExt" == "$ext" ]]; then
				return 0 # Extension is in list, process it!
			fi
		done
	fi
	return 1 # If it makes it this far, file extension wasn't found; ignore file
}

# Input: $1 - folder (full or relative path), $2 - current output folder, $3 - any text (see below)
# NOTE: If $3 is present, function assumes folder is a parent and ignores set options for folders
#       This functionality is meant to be used in main(), that way we can call this recursively for nested folders
# Returns: 0 for success, 1 for error
function processFolder() {
	local isParent=1
   local forget=1
	
	# Sanity check
	if [[ -z $1 ]]; then
		debug "l2" "FATAL: No folder given to processFolder()!"
		return 1
	elif [[ ! -d "$1" ]]; then
		debug "l2" "ERROR: $1 is not a folder, cannot process!"
		return 1
	elif [[ ! -z $3 ]]; then
		if [[ "$3" == "forget" ]]; then # Tells program not to touch outDir for one loop, effectively removing parent folder from new directory structure
         forget=0
      else
         debug "l5" "DBG: $1 is a parent, treating it as such!"
         isParent=0
      fi
   fi
   
   # Make sure $2 is a folder
   if [[ "$2" != */* ]]; then
      debug "l2" "ERROR: Arg 2 ($2) is not a folder, cannont continue!"
      return 1
	fi
	
   local OPWD="$(pwd)"
   local outDir="$2" # If $keepStructure is off, this will be the same. But, it's the easiest implementation I could think of
   if [[ "$isParent" -eq 1 && "$keepStructure" -eq 0 ]]; then
      if [[ "$forget" -eq 0 ]]; then
         debug "l5" "DBG: Ignoring outDir shift from parent..."
      else
         local name="$(printf "%s" "$1" | rev | cut -d'/' -f 2 | rev)"
         outDir="$outDir/$name" # Recursively adds on to each folder this way
         if [[ ! -d "outDir" ]]; then
            debug "WARN: Creating folder $name in $2 to keep structure!"
            if ! mkdir -p "$outDir"; then
               debug "l2" "ERROR: Could not create directories for $outDir! Do you have permission?"
               return 1
            fi
         fi
      fi
   fi # My sincere apologies to future me when you rediscover this nested if loop. Alas, it is required.
	
   cd "$1"
	
   for file in *; 
	do
		if [[ -d "$file" ]]; then
			if [[ "$isParent" -eq 0 ]]; then
				processFolder "$file" "$outDir" "forget"
			else
				if [[ $copyFolders -eq 0 ]]; then # Always complete condition checks, don't be lazy ( [ $cond ] vs [ $cond -eq 0 ] )
					debug "l5" "DBG: Copying $file to $outputDir"
					cp -R "$file" "$outDir"
				else # Enter folder and process
					debug "l5" "DBG: Processing contents of $file"
					processFolder "$file" "$outDir"
				fi
			fi
		else
			if ! processFile "$file" "$outDir"; then
				debug "l2" "WARN: Something went wrong while processing $file, see log for details!"
			fi
		fi # Lord forgive me for these nested if statements
	done
	
	# Finally, check if folder is empty and should be deleted
	cd "$OPWD"
	if [[ $deleteEmptyFolders ]]; then
		if [[ -z "$(ls -A "$1")" ]]; then
			debug "l5" "DBG: $1 is now empty, deleting folder!"
			if ! rmdir "$1"; then # I chose to use rmdir instead of `rm -rf` because the -f could accidentally delete non-empty folders
				debug "l2" "ERROR: Folder $1 could not be deleted! Please check permissions, or delete manually!"
				# For now, I see no need to do a return 1 here, since this is an unnecessary step anyways
			fi
		else
			debug "WARN: Folder $1 is not empty after processing, and will be left alone for now."
		fi
	fi
	return 0
}

# Input: $1 - a file, $2 - desired output folder
# Checks SHA hash of file, then deals with it accordingly
# Returns: 0 for success, 1 for error
function processFile() {
	if [[ -z $1 ]]; then
		debug "ERROR: No argument given to processFile()!"
		return 1
   elif [[ -z $2 ]]; then
      debug "l2" "ERROR: No folder given to processFile()! Ignoring $1..."
      return 1
	elif ! processExtension "$1"; then
		debug "l5" "DBG: Extension of $1 is not on the list, ignoring file..."
		return 0 # Technically a success
	fi
		
	local hash="$(sha256sum "$1" | cut -d' ' -f1)"
	local hashExists=1 # Assume no unless proven otherwise
	for h in "${hashList[@]}"; # This may throw an error on first run, keep an eye out. Just add an if statement to check for empty array to fix it, if so
	do
		if [[ "$h" == "$hash" ]]; then
			debug "l5" "DBG: Hash of $1 exists, dealing with file!"
			hashExists=0
			continue
		fi
	done
	
	if [[ $hashExists -eq 0 ]]; then
		# File already in main folder, delete the copy
		case "$deleteMode" in
			0)
			debug "l5" "DBG: Keeping existing file $1"
			;;
			1)
			debug "l5" "DBG: Moving file $1 to trash folder"
			if ! mv --backup=t "$file" "$trashFolder"; then
				debug "l2" "ERROR: File $1 could not be moved to trash $trashFolder! Please check permissions!"
				return 1
			fi
			;;
			2)
			debug "WARN: Permanently deleting file $1"
			if ! rm "$1"; then
				debug "l2" "ERROR: An error occurred, could not remove file $1!"
				return 1
			fi
			;;
		esac
	else # File does not exist, copy of move file and add hash to list
		hashList+=("$hash")
      
      # Make sure folder structure can be preserved before copying
      if [[ ! -d "$2" ]]; then
         debug "l5" "Output folder(s) $2 don't exist, attempting to make recursively..."
         if ! mkdir -p "$2"; then
            debug "ERROR: Could not create folders for $2! Do you have permission?"
            return 1
         fi
      fi
      
      # Finally, deal with file
		if [[ "$moveFiles" -eq 0 ]]; then
			debug "l5" "DBG: Moving the file $1 to $2"
			if ! mv "$1" "$2"; then
				debug "l2" "ERROR: File $1 could not be moved to output $2! Check permissions!"
				return 1
			fi
		else
			debug "l5" "DBG: Copying the file $1 to $2"
			if ! cp "$1" "$2"; then
				debug "l2" "ERROR: File $1 could not be copied to output $2! Check permissions!"
				return 1
			fi
		fi
	fi
	return 0 # Making it this far is success
}

function interactiveMode() {
   debug "INFO: Beginning interactive mode!"
   announce "On the following screen, please select the parent folder to process" "Press [TAB] to change panels, and [SPACE] twice to enter a directory in selection screen!"
   local pDir=""
   
   # Make sure user actually selects a folder
   while [[ -z $pDir || ! -d "$pDir" ]]; 
   do
      pDir="$(dialog --stdout --title="Select Parent Folder" --dselect . 14 48)"
   done
   
   local OPWD="$(pwd)"
   cd "$pDir"
   
   # Add all the folders to a tmp doc to be worked
   # TODO: Add file support here as well
   #declare -a fList
   local fList="$pDir"/".$(pwd)_consolidate).tmp" # Should be a unique enough name
   for item in *; do
      if [[ -d "$item" ]]; then
          echo "$item" | tee -a "$fList" 1>/dev/null # Full directory path in case I want to parallel-ize this script later
      fi
   done
   
   announce "In the next screen, comment out (or delete) any folders you don't want processed"
   editTextFile "$fList"
   
   # Now put the fList into the folderList array, and delete tmp file
   
   # Prompt user to input remaining values needed for script to run
   
   # TODO: Warn user if outputFolder/trashFolder DNE and ask to create
}

### Main

checkRequirements "dialog" "rmdir/coreutils"
processArgs "$@"

# Make sure output folder exists before moving on
if [[ "$outputFolder" != /* ]]; then # Turn the relative path into a full path
	outputFolder="$(pwd)/$outputFolder"
fi
if [[ ! -d "$outputFolder" ]]; then
	debug "l2" "WARN: Output folder $outputFolder does not exist, attempting to create!"
	if ! mkdir "$outputFolder"; then
		debug "l2" "FATAL: Directory could not be created! Please check permissions and try again!"
		exit 1
	fi
fi

# Now, do all the work
if [[ ${#folderList[@]} -eq 0 ]]; then
	debug "l2" "FATAL: Script has no folders to work on! Please fix and re-run!"
	displayHelp
	exit 1
else
	for folder in "${folderList[@]}";
	do
		if ! processFolder "$folder" "$outputFolder" "parent"; then
			debug "l2" "ERROR: An error occurred while processing parent folder $folder! Check log for details!"
		fi
	done
fi

#echo "extList is: ${extList[@]}"
debug "l3" "INFO: Done with script!"
#EOF