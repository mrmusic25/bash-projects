#!/bin/bash
#
# musicCrawler.sh - A script that traverses a media folder and creates a CSV database of all the contained music
#
# Usage: ./musicCrawler.sh [options] <folder> <database.csv>
#
# Format of CSV (comma-delimited): File location, SHA256, artist, album, song title, genre, bitrate
#
# TODO:
# - Create processArgs(), iff the need arises
#
# Changelog:
# v1.1.2
# - Fixed calculation
# - Added average songs per second to output
# - Got rid of second unnecessary time check
#
# v1.1.1
# - Added displayProgress() because I was tired of not knowing whether the script was still working
# - Will also help with speed diagnosis
# - Default reporting every 180 seconds, change reportSeconds to liking
#
# v1.1.0
# - Switched script from using eyeD3 to efixtool (more efficient, and easier to extract info)
# - Reduced number of debug statements as it slowed down processing (~23 seconds per song on average)
# - Added bitrate to export since it was easy to implement
# - Also added genre
#
# v1.0.1
# - Added a check for valid music extension
#
# v1.0.0
# - First release version!
# - Fixed some silly bugs
# - Output works now, even in Windows
#
# v0.2.0
# - Added importDatabase()
# - Finished writing other functions
# - Script is "complete" and ready for testing
#
# v0.1.0
# - Added processFolder(), processFile(), and fileExists()
# - Dealt with ancient backup files
# - Changed anticipated format of CSV file, for future use
#
# v0.0.1
# - Initial commit
#
# v1.1.2, 18 June 2019 14:39 PDT

source /usr/share/commonFunctions.sh

### Vars

inputFolder=""
databaseFile=""
forceNew=1 # 0 for true, -n|--new-database option
imported=1 # Flag to tell if database has been imported already to avoid overwriting data (0 == true)
declare -a databaseContents # It is faster to work with an array in memory than waiting on the hard copy. More accurate, too.
allowedExt=(mp3 m4a m4p aac wma ogg wav flac) # List of allowed extensions

# Vars for progress
reportSeconds=180 # Time between updates
lastUpdate=0
folderCount=0
songCount=0

### Functions

function displayHelp() {
read -d '' helpVar <<"endHelp"

musicCrawler.sh - A script that traverses a media folder and creates a CSV database of all the contained music

Usage: ./musicCrawler.sh [options] <folder> <database.csv>
	
Options:
  -h | --help                             : Shows this help message and exits
  -v | --verbose                          : Enables verbose debug messaging (MUST be first argument!)
  -n | --new-database                     : Force creates a new database, even if there is already an existing one

NOTE: While not designed as such, this script can be modified to catalog an entire folder in CSV
      By default, an existing database will be verified and appended to

endHelp
echo "$helpVar"
}

# Usage: processFolder <folder>
function processFolder() {
	#debug "l5" "DBG: Beginning work on $1"
	if [[ -z $1 ]]; then
		debug "l2" "ERROR: No argument given to processFolder()!"
		return 1
	elif [[ ! -d "$1" ]]; then
		debug "l2" "ERROR: Arg $1 given to processFolder() is not a folder!"
		return 1
	fi
	
	# Onto the work!
	local OOPWD="$(pwd)"
	cd "$1"
	for file in *; do
		if [[ -d "$file" ]]; then
			if ! processFolder "$(pwd)"/"$file"; then
				debug "l2" "ERROR: There was a problem processing folder $file!"
			fi
		elif [[ -f "$file" ]]; then
			if ! processFile "$(pwd)"/"$file"; then
				debug "l2" "ERROR: $file could not be processed!"
			fi
		else
			debug "ERROR: Unexpected element $file found in $(pwd)! Attempting to continue..."
		fi
		((songCount++))
	done
	cd "$OOPWD"
	((folderCount++))
	[[ $((SECONDS - lastUpdate - reportSeconds)) -gt 0 ]] && displayProgress
	return 0
}

# Usage: processFile <file>
function processFile() {
	# Check if arg present -> arg is a file -> arg is full path
	local f="$1"
	if [[ -z $f ]]; then
		debug "ERROR: No argument given to processFile()"
		return 1
	elif [[ ! -f "$f" ]]; then
		debug "ERROR: Arg $1 given to processFile() is not a file!"
		return 1
	elif [[ "$f" != /* ]]; then
		#debug "WARN: $1 is not a full path, attempting to use pwd..."
		f="$(pwd)"/"$f"
		if [[ ! -f "$f" ]]; then
			debug "ERROR: Full path could not be determined, please fix manually!"
			return 1
		fi
	fi
	
	# File exists, not check to see if file is in database already
	if fileExists "$f"; then
		# No l5 debug here because it is already in fileExists()
		return 0 
	fi # TODO: Check and report if SHA of song has changed since last import. Or maybe should be per-script?
	
	# Now, extract data
	#local id3="$(eyeD3 "$f" 2>/dev/null)"
	local title="$(efixtool -Title "$f" 2>/dev/null | cut -d':' -f2)"
	local artist="$(efixtool -Artist "$f" 2>/dev/null | cut -d':' -f2)"
	local album="$(efixtool -Album "$f" 2>/dev/null | cut -d':' -f2)"
	local bitrate="$(efixtool -AvgBitrate "$f" 2>/dev/null | cut -d':' -f2)"
	local genre="$(efixtool -Genre "$f" 2>/dev/null | cut -d':' -f2 | cut -d' ' -f1)" # Second cut gets rid of 'kbps' at the end
	local sum="$(sha256sum "$f" | cut -d' ' -f1)"
	
	# I hate doing this, but allowing the original string is necessary
	if [[ "$(echo "$title" | grep -c ,)" -gt 0 ]]; then
		title=\""$title"\"
	fi
	if [[ "$(echo "$artist" | grep -c ,)" -gt 0 ]]; then
		artist=\""$artist"\"
	fi
	if [[ "$(echo "$title" | grep -c ,)" -gt 0 ]]; then
		album=\""$album"\"
	fi
	
	# Finally, concatenate it all and add it to databaseContents[]
	local str="$f","$sum","$artist","$album","$title","$genre","$bitrate"
	#debug "l5" "DBG: Adding $str to databaseContents[]"
	databaseContents+=("$str")
	return 0
}

# Usage: fileExists <file>
# Checks to see if file already exists in database (0 == true)
function fileExists() {
	local f="$1"
	if [[ -z $f ]]; then	
		debug "l2" "ERROR: No argument given to fileExists()! Attempting to continue..."
		return 1
	elif [[ ! -f "$f" ]]; then
		debug "l2" "ERROR: Arg $1 given to fileExists() is not a file! Continuing..."
		return 1
	elif [[ "$f" != /* ]]; then
		#debug "WARN: $1 is not a full path, attempting to use pwd..."
		f="$(pwd)"/"$f"
		if [[ ! -f "$f" ]]; then
			debug "ERROR: Full path could not be determined, please fix manually!"
			return 1
		fi
	fi
	
	# Check to make sure database is populated
	if [[ -z ${databaseContents[@]} ]]; then
		debug "l2" "WARN: Database file is empty, assuming its on purpose and continuing"
		return 1
	fi
	
	# Make sure extension is allowed
	local e="$(echo "$f" | rev | cut -d'.' -f1 | rev)"
	if [[ "$(echo "${allowedExt[@]}" | grep -m1 -i -c "$e")" -eq 0 ]]; then
		#debug "WARN: File $f is not an allowed extension and will be skipped!"
		return 0 # Technically a success
	fi
	
	# Now, see if line exists in database already
	# Originally I was gonna have a for loop do this, for more control, but grep just seemed more efficient if I would be calling it thousands of times in one script
	local c="$(echo "${databaseContents[@]}" | grep -m1 -i -c "$f")" # NTFS is case-insensitive, hence the -i flag
	if [[ $c -eq 1 ]]; then
		#debug "l5" "DBG: Song $f exists in database! Continuing..."
		return 0
	#elif [[ $c -gt 1 ]]; then
	#	debug "l2" "WARN: There appear to be multiple copies of $f in the database. Please fix manually!"
	#	return 0
	fi # TODO: Run tests to see if including this elif is worth the CPU time
	return 1 # Assumes $c=0
}

function importDatabase() {
	if [[ $imported -eq 0 ]]; then
		debug "l2" "WARN: Database has already been imported, skipping new import!"
		return 0
	elif [[ ! -f "$databaseFile" ]]; then
		debug "l2" "WARN: Database file does not exist, and could not be imported!"
		imported=0 # Assume new database is being made
		return 0
	else
		debug "l5" "DBG: Database will now be imported, setting flag..."
		imported=0
	fi
	
	while read -r line
	do
		databaseContents+=("$line")
	done < "${databaseFile}"
	return 0
}

function displayProgress() {
	local sec=$((SECONDS - lastUpdate))
	#if [[ $sec -lt 0 ]]; then
	#	return 1 # Not time to report yet
	#fi
	
	printf "~~~ %s songs processed in %s folders in the last %s seconds (%s per second avg.) ~~~\n" "$songCount" "$folderCount" "$sec" "$((songCount / sec))"
	lastUpdate="$SECONDS"
	songCount=0
	folderCount=0
	return 0
}

### Main

checkRequirements "exiftool" #"eyeD3" # Used for extracting ID3 data

# Remember to delete these next couple lines if/when processArgs() is implemented
if [[ "$1" == *-h* ]]; then
	displayHelp
	exit 0
elif [[ "$1" == *-n* ]]; then
	debug "WARN: Old database will be deleted without confirmation!"
	forceNew=0
	shift
fi
	

# Make sure folder is valid, and create database (warn if database already exists)
OPWD="$(pwd)"
if [[ ! -d "$1" ]]; then
	debug "l2" "FATAL: $1 is not a valid directory! Please fix and run again!"
	displayHelp
	exit 1
elif [[ "$1" == . ]]; then
	debug "INFO: Using pwd $(pwd) as directory to catalog!"
	inputFolder="$(pwd)"
else
	inputFolder="$1"
fi

if [[ "$2" != *.csv ]]; then
	debug "l2" "ERROR: Given file $2 is not a valid csv file!"
	displayHelp
	exit 1
fi
databaseFile="$2"

if [[ "$forceNew" -eq 0 ]]; then
	debug "l2" "WARN: Force creating new database!"
	if [[ -f "$databaseFile".bak ]]; then
		debug "WARN: Deleting ancient database file!"
		if ! rm "$databaseFile".bak; then
			debug "l2" "ERROR: Could not remove ancient database, do you have permissions? Attempting to continue..."
		fi
	fi
	if ! mv "$databaseFile" "$databaseFile".bak; then
		debug "l2" "ERROR: Database $databaseFile did not exist, so it could not be deleted. Continuing!"
	fi
elif [[ -f "$databaseFile" ]]; then
	debug "l2" "WARN: $databaseFile already exists, verifying and appending existing database!"
	#TODO: Verify database (delete missing entries, ensure current entries still accurate)
fi

# Finally, the moment you've all been waiting for:
importDatabase
processFolder "$inputFolder"
printf "%s\n" "${databaseContents[@]}" > "$databaseFile" # https://superuser.com/questions/461981/how-do-i-convert-a-bash-array-variable-to-a-string-delimited-with-newlines

cd "$OPWD"
debug "l2" "INFO: Done with script!"
#EOF