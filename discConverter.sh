#!/usr/bin/env bash
#
# discConverter.sh - A script to convert CD/DVD/BD-ROMs to compressed ISO files
#
# Changes:
# v0.4.0
# - Fixed cc() so it functions properly
#
# v0.3.1
# - After a couple days of experimentation and trying different programs, I settled on using ddrescue
# - I learned that ddrescue is not meant for CD-ROMs with scratches and stuf, but no other program can recover as much data
# - So, I am simply going to harvest as much data as possible, then report the lost data
# - Most CDs supposedly have error checking built in so it should be fine
# - Only one run now, with a timeout for when it inevitably runs into read errors so it does not run endlessly
# - Added a function to trap and process a CTRL+C event
#
# v0.3.0
# - Updated the text in the help section
# - Added the ability to automatically store keys for discs in the same archive
# - Sets of discs can now use the same archive
# - Script will ask for names to append to the different discs
# - Changed a few debug messages
# 
# v0.2.6
# - Jumped the gun and fixed an issue that wasn't there. Actually fixed it this time
# - Made sure everything it using variable names instead of static
# - Added many more comments so finding points in my code is easier
# - Fixed some debug messages
#
# v0.2.5
# - Fixed a boolean error preventing 7zip from being run
#
# v0.2.4
# - Added another round of ddrescue after consulting ArchWiki (https://wiki.archlinux.org/index.php/Disk_cloning#Using_ddrescue)
# - Moved everything to variables for extensibility
# - Even more verbosity
# - Fixed a grammar mistake
#
# v0.2.3
# - dum. 
# - Fixed the main loop that never triggered
#
# v0.2.2
# - Script will now offer to loop until stopped, for backup purposes
# - More verbosity
# - Made 7z quieter by supressing stdout (leaving ddrescue for now since stats are important)
#
# v0.2.1
# - Limit retries per sector of ddrescue (-r3 flag) since it was taking 4+ hours for a single CD-ROM
# - Announce to user when ddrescue will run since it will take more time
#
# v0.2.0
# - Added UID for changing ownership
# - Also added argument to change the UID from default
# - Added ddrescue for failed discs since 2/3 of the discs I tried failed before finishing
# - Added ability to use output directory instead of current directory
# - Converted more static names to variables
# - Increased verbosity
#
# v0.1.2
# - Added the -x flag for 'isosize', oops lol
#
# v0.1.1
# - Changed position of the privilege check
# - Script will eject drive and then prompt user so they know which disc is going in which drive
# 
# v0.1.0
# - This will be initial release version, trying to follow semantice versioning from now on
# - Added processDrives, where all the work is done
# - Fixed errors in shellcheck
# - Ready for testing/release!
#
# v0.0.1
# - Initial version
# - Filled out help and initial options in prep for script
#
# TODO:
# - Detect music CDs and offer to rip them
#   ~ Options for format (mp3, FLAC, and m4a probably)
#   ~ Get track names from database?
# - Add more forms of compression 
#   ~ Just 7z for now
# - Change user/group of items (since this needs root access)
#   ~ Alternatively, run everything as sudo? Would need to be monitored if so...
# - Save everything with myConfig (global) so only script name needs to be run
# - Check available space before copying disc/compressing
# - Options:
#   ~ -q|--quiet to make 7z supress stdout
#   ~ -qq|--qquiet to make ddrescue and dd quiet too
#   ~ -s|--single-run to turn off loop flag and only run script once
#   ~ -n|--names to provide the names for the respective discs (${var#} must match $numDiscs)
#   ~ -t|--timeout to specify the timeout for ddrescue (static at 10m for now)
# - Ask all questions at once and store so processing is more hands-off
#   ~ Maybe store everything in a temporary CSV file?
#   ~ Disc set, disc num, key
#   ~ Maybe ask if the next disc in the set is part of the set?
#
# v0.3.1, 15 Feb. 2021, 02:28 PST

### Setup

export longName="discConverter"
export shortName="DC"
export aChar="C"

if ! source /usr/share/commonFunctions.sh; then
    if ! source commonFunctions.sh; then
        echo "ERROR: commonFunctions.sh not found! Please install from https://gitlab.com/mrmusic25/linux-pref and re-run script!"
        exit 1
    fi
fi

### Variables

numDrives=1                          # Number of CD-ROM drives attached to computer
drivePrefix=/dev/sr                  # Starting prefix of CD-ROM drives (/dev/sr[0], /dev/cdrom[0], etc.)
#compMode=gz                          # Method of compression to use (gz, 7z, xz)
deleteISO=0                          # Whether or not to delete the ISO after compression (on by default)
chownUID="1000:1000"                 # UID of the user to change ownership to
outputDir="."                        # Directory where file will be stored
tmuxFile="$(pwd)/.tmux.txt"          # Text file where the names of running tmux sessions is stored
runningTasks="$(pwd)/.dcTasks.txt"   # List of the lock files currently in use
compQueue="$(pwd)/.dcQueue.csv"      # Comma-delimited list of files waiting to be compressed

### Functions

function displayHelp() {
# The following will read all text between the words 'helpVar' into the variable $helpVar
# The echo at the end will output it, exactly as shown, to the user
read -d '' helpVar <<"endHelp"

.sh - discConverter.sh - An interactive script to convert CD/DVD/BD-ROMs to compressed ISO files

Usage: ./discConverter.sh [options] 

Options:
-h | --help                     : Display this help message and exit
-v | --verbose                  : Enables verbose logging. Use -vv | --vverbose for extra debug info.
-n | --num-drives <num>         : Number of physical CD drives you will be using (number is appended to prefix)
-p | --prefix </dev/xy>         : Prefix of your cdrom drives. Default is /dev/sr, sometimes /dev/cdrom, depends on distrobution         
-d | --delete-iso [true/false]  : Turn on/off deletion of the original ISO after compression. Default if on
-o | --output <directory>       : Directory where compressed files will be sent
-u | --user <user[:group]>      : Specify the user/group to change ownership of the files to. If only user given, assume user:user. UID also with same principle.

Note:
   It is recommended to run this script in either a screen or tmux session so you can let it run in the background!
      This will be automatically implemented into the script one day.

   Some distros use /dev/sr0, /dev/sr1, etc. for CD-ROM drives. Others use /dev/cdrom0, /dev/cdrom1, etc. Make sure to doublecheck yours and use the proper format!
      e.g I have 3 CD drives on /dev/sr0, /dev/sr1, and /dev/sr2. I would use '--prefix /dev/sr --num-drives 3' to utilize all 3.
   
   If a dd fails reading a disc, it will fallback to ddrescue: this WILL take a long time! ~2 hours for a CD-ROM, ~3 for a DVD-ROM, and ~5 for a BD-ROM
      Please plan accordingly, as disc drives can get noisy while script is running them.
	  You can always CTRL+C to skip the disc and try again when you have time.

   Discs part of a set will be output in the '/dir/Disc_Name_Disc-XYZ.iso' where XYZ is the tag you give the disc interactively.
      This allows both numbered sets (disc 1, disc 2, etc.) and names sets ('Game disc', 'Data disc', etc.)
	  These disc sets will all use the same archive file, so be sure to set it the same name!
	  Make sure to only include the key once when using sets as well!


endHelp
echo "$helpVar"
}

function processArgs() {
	if [[ $# -lt 1 ]]; then
		debug "l2" "No arguments given to processArgs()! Attempting to run with defaults..."
		return 0
	fi
	
	while [[ -n $1 ]]; do
		key="$1"
			
		case "$key" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-v|--verbose)
			export debugLevel=1
			debug "l1" "Verbose logging enabled"
			;;
			-vv|--vverbose)
			export debugLevel=0
			debug "l0" "Debug logging enabled"
			;;
			-n|--num-drives)
			if [[ -z $2 || "$2" == -* ]]; then
				debug "l4" "No argument given with $key! Please fix and re-run!"
				return 1
			elif [[ $2 -ne $2 ]]; then
				debug "l3" "Argument $2 is not a number! Please fix and re-run!"
				return 1
			else
				debug "l1" "Setting number of drives to $2"
				numDrives="$2"
				shift
			fi
			;;
			-p|--prefix)
			if [[ -z $2 || "$2" == -* ]]; then
				debug "l4" "No argument given with $key! Please fix and re-run!"
				return 1
			fi
			if [[ "$2" != /dev/* ]]; then
				debug "l3" "Location of drive should start with /dev, received $2! Please fix and re-run!"
				return 1
			elif [[ -e "$2" ]]; then
				debug "l2" "$2 is an actual drive, not a prefix! Using ${2%?} instead!"
				drivePrefix="${2%?}"
			else
				debug "l1" "Using $2 as the drive prefix!"
				drivePrefix="$2"
			fi
			shift
			;;
			-d|--delete-iso)
			if [[ -z $2 || "$2" == -* ]]; then
				debug "l4" "No argument given with $key! Please fix and re-run!"
				return 1
			fi
			if yesorno "$2"; then
				debug "l1" "Enabling ISO deletion"
				deleteISO=0
			else
				debug "l1" "Disabling ISO deletion"
				deleteISO=1
			fi
			shift
			;;
			-o|--output)
			if [[ -z $2 || "$2" == -* ]]; then
				debug "l4" "No argument given with $key! Please fix and re-run!"
				return 1
			elif [[ "$2" != /* ]]; then
				debug "l2" "$2 is not a full path, assuming present directory..."
				outputDir="$(pwd)/$2"
			else
				outputDir="$2"
			fi
			
			debug "l1" "Set output directory to $outputDir"
			if ! mkdir --parents "$outputDir"; then
				debug "l4" "Could not create full path $outputDir, does $USER have permission? Please fix and retry!"
				return 1
			fi
			shift
			;;
			-u|--user|--uid)
			if [[ -z $2 || "$2" == -* ]]; then
				debug "l4" "No argument given with $key! Please fix and re-run!"
				return 1
			fi
			if [[ "$2" != *:* ]]; then
				debug "l2" "Only user $2 given, assuming group of the same name!"
				chownUID="$2:$2"
			else
				chownUID="$2"
			fi
			
			debug "l1" "Set UID to $2"
			shift
			;;
			*)
			debug "l4" "Unknown option given: $key! Please fix and re-run!"
			return 1
			;;
		esac
		shift
	done
}

function processDrives() {
	declare -a outputNames

	# Get filenames
	local count=0
	local n
	while [[ $count -lt $numDrives ]]; do
		eject "$drivePrefix$count"
		pause "Ejected drive $drivePrefix$count, put a disc in the ejected drive then press [Enter]: "
		read -p "Enter the filename (without the file extension and disc number) for $drivePrefix$count: " n
		outputNames+=("$n")
		((count++))
	done

	# Variables
	count=0
	local blockSize=2048
	local sectorCount=0
	local disc             # Current disc drive being worked on
	local finalOutput      # Output of the final compressed disc
	local imageOutput      # Location of the image file
	local mapOutput        # Map file, in case ddrescue needs to be used
	local actKey="null"    # Activation key for the disc, if it has one
	local keyFile          # File where the key will be stored

	# Process each name with its respective disc
	for name in "${outputNames[@]}"; do
		if ! isosize "$drivePrefix$count" 1>/dev/null; then
			debug "l3" "Could not open drive $drivePrefix$count, is a disc inserted? Attempting to continue, code=$?"
		else
			# Set the data for this run
			disc="$drivePrefix$count"
			imageOutput="$outputDir/$name.iso"
			mapOutput="$outputDir/$name.map"
			finalOutput="$imageOutput.7z"
			keyFile="$outputDir/$name-Key.txt"
			
			# Ask if disc is part of a multipack
			if getUserAnswer "Is this disc part of a multi-disc set?"; then
				read -p "Enter the number or tag for this disc (e.g. 2, \'Game_Disc\', etc.) and press [Enter]: " imageOutput 
				imageOutput="$outputDir/$name""_Disc-$imageOutput.iso"
			fi

			# Ask if there is a key. Do not write it to logs for security reasons!
			if getUserAnswer "Is there an activation/license key you would like to include with this disc?"; then
				read -p "Enter the key and press [Enter]: " actKey
			else
				actKey="null"
			fi

			# Get disk info for dd
			isosize -x "$disc" > "$name.tmp"
			sectorCount="$(cat "$name.tmp" | cut -d',' -f1 | cut -d':' -f2 | xargs)"
			blockSize="$(cat "$name.tmp" | cut -d',' -f2 | cut -d':' -f2 | xargs)"
			rm "$name.tmp"

			# Copy the disc, using dd first
			debug "l1" "Attempting to read disc $disc to $imageOutput with block size $blockSize and sector count $sectorCount!"
			if ! dd if="$disc" of="$imageOutput" bs="$blockSize" count="$sectorCount" status=progress; then
				debug "l3" "An error occurred while processing $disc! Trying with ddrescue..."
				rm "$imageOutput" # Just in case anything made it
				announce "Attempting to copy $disc with ddrescue!" "This WILL take a long time, please be patient!" "Sectors that fail 3 times will be ignored, data may lose integrity!"
				
				# If dd fails, use ddrescue
				if ! ddrescue -d -T 10m -n -r1 -b "$blockSize" -s "$((blockSize*sectorCount))" "$disc" "$imageOutput" "$mapOutput" | tee dd.log; then
					debug "l2" "An error occurred while running ddrescue, likely read errors. Not deleting map, adding to log!"
					cat "dd.log" >> "$logFile"
					debug "l2" "If you want to try and recover more data from this disc, run the following command:"
					debug "l2" "   ddrescue -d -r3 -b $blockSize -s $((blockSize*sectorCount)) $disc $imageOutput $mapOutput"
				else
					debug "l1" "Successfully ran recovery on disc, removing unneeded map file!"
					rm "$mapOutput"
				fi
				rm "dd.log"

			# Now, compress the image
			elif [[ -f "$imageOutput" ]]; then
				debug "l1" "Successfully ripped $disc to $imageOutput! Attempting to compress..."
				if ! 7z a -t7z "$finalOutput" -m0=lzma2 -mx=9 "$imageOutput" 1>/dev/null; then
					debug "l3" "Could not compress $imageOutput! Skipping deletion, please manually compress!"
				else
					
					# Delete the original image file, if enabled
					if [[ "$deleteISO" -eq 0 ]]; then  #spaghet
						debug "l1" "Deleting ISO $imageOutput after successful compression!"
						if ! rm "$imageOutput"; then
							debug "l3" "Could not delete $imageOutput! Please do so manually, continuing..."
						fi
					else
						debug "l1" "Skipping deletion of $imageOutput..."
					fi
					debug "l1" "Done compressing $finalOutput!"

					# Export the key
					if [[ "$actKey" != "null" ]]; then
						debug "l1" "User provided a key with $imageOutput, exporting to text and including in $finalOutput!"
						echo "$actKey" > "$keyFile"
						if ! 7z a -t7z "$finalOutput" -m0=lzma2 -mx=9 "$keyFile" 1>/dev/null; then
							debug "l3" "Could not add key file to the existing archive $finalOutput! Please manually backup key!"
						else
							debug "l1" "Successfully added key to $finalOutput!"
						fi

						rm "$keyFile"
					fi
				fi
			else
				debug "l2" "No file with name $imageOutput found, assuming process failed! Continuing..."
			fi
		fi
		
		# Change owner, increment, and continue
		if ! chown "$chownUID" "$finalOutput"; then
			debug "l2" "Couldn't change ownership of $finalOutput to $chownUID, please do so manually!"
		fi

		((count++))
	done
	debug "l1" "Finished running all discs!"
	return 0
}

# importDisc <csv-style-line> [rescue]
# Format: <disc_drive>,<output_file>,<disc_num>,<key_file>
# If rescue is present, re-run with ddrescue
function importDisc() {
	# Sanity check
	if [[ -z $1 ]]; then
		debug "l4" "No argument given to importDisc()! Please fix and re-run!"
		return 1
	elif [[ "$1" != *,* ]]; then
		debug "l4" "Argument $1 is not a comma-delimited string! Please fix and re-run!"
		return 1
	elif [[ "$(echo "$1" | sed -e 's/[^,]//g')" -ne 4 ]]; then
		debug "l4" "Wrong number of arguments in CSV string $1! Please fix and re-run!"
		return 1
	fi

	# Set the variables
	local discDrive="$(echo "$1" | cut -d',' -f1)"
	local baseFile="$(echo "$1" | cut -d',' -f2)"
	local discNum="$(echo "$1" | cut -d',' -f3)"
	local lockFile="$(pwd)/.dc$(echo "$discDrive" | sed -e 's/\//-/g').lock"
	local resultFile="$(pwd)/.dc$(echo "$discDrive" | sed -e 's/\//-/g').rtn"
}

# Function to trap CTRL+C
function cc() {
	if [[ -z $ccSeconds || $((SECONDS-ccSeconds)) -gt 5 ]]; then
		export ccSeconds="$SECONDS"
		debug "l2" "Caught a signal, assuming it was used to stop ddrescue! Press again within 5 seconds to stop script!"
		return 0
	else
		debug "l2" "Exiting script due to second caught signal!"
		exit 1
	fi
}

### Main Script

# Putting it here so the whole script uses it
trap cc SIGINT

#checkRequirements "7z/p7zip-full"

if ! processArgs "$@"; then
	debug "l4" "An error occurred while processing arguments, please fix and re-run!"
	displayHelp
	exit 1
fi

# Make sure script has root access for CD drives
if [[ "$UID" -ne 0 ]]; then
	debug "l2" "This script requires root privileges! Attempting to run with sudo..."
	if ! eval "sudo $0 $*"; then
		debug "l3" "An error occurred while running the script! Login as root and view the log for more info!"
		exit 1
	else
		debug "l1" "Script finished successfully!"
		exit 0
	fi
fi

flag=0
while [[ flag -eq 0 ]]; do
	processDrives
	if ! getUserAnswer "Would you like do another run?"; then
		flag=1
	fi
done

debug "l1" "Done with script!"
exit 0

#EOF